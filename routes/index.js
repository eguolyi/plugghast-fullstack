

var express = require('express');
var router = express.Router();
const fetch = require("node-fetch");

const REQUEST_WORD_TYPE = 'lookupword'
const REQUEST_COMPLETION_TYPE = 'generatecompletion'

const MSAPI_REQUEST_DICT_TYPE = 'dictionary'
const MSAPI_REQUEST_TRANS_TYPE = 'translate'

// look up a word
router.get('/lookupword/:keyWord', function(req, res, next) {
  fetchDataFromFolkLexikon(REQUEST_WORD_TYPE, req.params.keyWord, res)
});

// get suggestions
router.get('/generatecompletion/:keyWord', function(req, res, next) {
  fetchDataFromFolkLexikon(REQUEST_COMPLETION_TYPE, req.params.keyWord, res)
});

// look up a word via ms api
router.get('/dictionary/:srcLang/:toLang/:keyWord', function(req, res, next) {
  fetchDataFromMsApi(MSAPI_REQUEST_DICT_TYPE, req.params.keyWord, req.params.srcLang, req.params.toLang, res)
});

// translate text via ms api
router.get('/translate/:srcLang/:toLang/:text', function(req, res, next) {
  fetchDataFromMsApi(MSAPI_REQUEST_TRANS_TYPE, req.params.text, req.params.srcLang, req.params.toLang, res)
});

// get inflection from SAOL
router.get('/inflection/:keyWord', function(req, res, next) {
  fetchDataFromSAOL(req.params.keyWord, res)
});

/*
  Google Translate api call
 */
const projectId = 'plugghast-1162';
const location = 'global';

// Imports the Google Cloud Translation library
const {TranslationServiceClient} = require('@google-cloud/translate').v3beta1;

// Instantiates a client
const translationClient = new TranslationServiceClient();
async function googleTranslateText(text, srcLang, toLang, res) {
  // Construct request
  const request = {
    parent: translationClient.locationPath(projectId, location),
    contents: [text],
    mimeType: 'text/plain', // mime types: text/plain, text/html
    sourceLanguageCode: srcLang,
    targetLanguageCode: toLang,
  };

  // Run request
  const [response] = await translationClient.translateText(request);

  let output = ''
  for (const translation of response.translations) {
    output += ((output === '' ? '' : ', ') + translation.translatedText)
  }

  res.send(output);
}

// get google translate text
router.get('/googletranslate/:srcLang/:toLang/:text', function(req, res, next) {
  googleTranslateText(req.params.text, req.params.srcLang, req.params.toLang, res)
});


function fetchDataFromFolkLexikon(requestType, keyWord, res) {
  const urlFolkLexikon = "http://folkets-lexikon.csc.kth.se/folkets/folkets/"
  const url = urlFolkLexikon + requestType
  var body_head, body_tail

  if (requestType === REQUEST_WORD_TYPE) {
    body_head = '7|0|6|http://folkets-lexikon.csc.kth.se/folkets/folkets/|1F6DF5ACEAE7CE88AACB1E5E4208A6EC|se.algoritmica.folkets.client.LookUpService|lookUpWord|se.algoritmica.folkets.client.LookUpRequest/1089007912|'
    body_tail = '|1|2|3|4|1|5|5|1|0|0|6|'
  } else {
    body_head = '7|0|7|http://folkets-lexikon.csc.kth.se/folkets/folkets/|72408650102EFF3C0092D16FF6C6E52F|se.algoritmica.folkets.client.ItemSuggestService|getSuggestions|se.algoritmica.folkets.client.ProposalRequest/3613917143|com.google.gwt.user.client.ui.SuggestOracle$Request/3707347745|'
    body_tail = '|1|2|3|4|1|5|5|0|6|5|7|'
  }


  return fetch(url, {
    body: body_head
        + keyWord
        + body_tail,
    headers: {
      'Content-Type': 'text/x-gwt-rpc; charset=UTF-8',
      'X-GWT-Permutation': 'D88EC72BE0FA10F91FDC911C7757B7F3',
      'X-GWT-Module-Base': 'http://folkets-lexikon.csc.kth.se/folkets/folkets/'
    },
    method: 'POST'
  })
      .then(response => response.text())
      .then(contents => {
        console.log(contents)
        res.send(contents)
      })
}

function fetchDataFromMsApi(requestType, keyWord, srcLang, toLang, res) {
  const urlMsApi = "https://api.cognitive.microsofttranslator.com/"
  const url = urlMsApi + requestType + (requestType === MSAPI_REQUEST_DICT_TYPE ? '/lookup' : '') + '?api-version=3.0&from=' + srcLang + '&to=' + toLang

  fetch(url, {
    body: JSON.stringify([{
      text: keyWord
    }]),
    headers: {
      'Ocp-Apim-Subscription-Key': '28a0e380347647edac758d54bed68c3f',
      'Content-type': 'application/json'
    },
    method: 'POST'
  })
      .then(response => response.json())
      .then(contents => res.send(contents));
}

function fetchDataFromSAOL(keyWord, res){
  const urlSAOL = 'https://svenska.se/tri/f_saol.php?sok=' + keyWord
  const urlEncoded = encodeURI(urlSAOL)
  console.log('saol' + urlEncoded)

  fetch(urlEncoded)
      .then(response => response.text())
      .then(contents => res.send(contents));
}

module.exports = router;
