var express = require('express');
var router = express.Router();
var app = express()

/* api only for testing purpose*/
router.get('/:keyWord', function(req, res, next) {
  res.send('tested with the requested word: ' + req.params.keyWord);
});

module.exports = router;