import { isObjectEmpty, timeoutableFetch } from '../common/util'

export const SELECT_DATA_SOURCE = 'SELECT_DATA_SOURCE'
export const DATA_SOURCE_FOLKLEX = 'folkLexikon'
export const DATA_SOURCE_MSAPI = 'msApi'
export const DATA_SOURCE_GAPI = 'googleApi'
export const DATA_SOURCE_SAOL = 'saol'

export const LOOKUP_KEY = 'LOOKUP_KEY'
export const REQUEST_DETAILS = 'REQUEST_DETAILS'
export const RECEIVE_DETAILS = 'RECEIVE_DETAILS'
export const REQUEST_FAILURE = 'REQUEST_FAILURE'
export const MARK_REQUEST_STATUS = 'MARK_REQUEST_STATUS'
export const SHOW_REQUEST_FAILURE = 'SHOW_REQUEST_FAILURE'
export const SET_NATIVE_LANGUAGE = 'SET_NATIVE_LANGUAGE'
export const SET_UI_LANGUAGE = 'SET_UI_LANGUAGE'
export const CLEAN_HISTORY_DATA = 'CLEAN_HISTORY_DATA'
export const SHOW_FUZZY_DIALOG = 'SHOW_FUZZY_DIALOG'
export const SET_TEXT_TRANSLATE_ENGINE = 'SET_TEXT_TRANSLATE_ENGINE'

export const REQUEST_WORD_TYPE = 'lookupword'
export const REQUEST_COMPLETION_TYPE = 'generatecompletion'

export const MSAPI_REQUEST_DICT_TYPE = 'dictionary'
export const MSAPI_REQUEST_TRANS_TYPE = 'translate'

export const GAPI_REQUEST_TRANS_TYPE = 'googletranslate'

//It is actually translate request but we have to store it in the store separately, otherwise it will be mixed up with the translate data in Dictionary Page
export const MSAPI_REQUEST_TRANS_PAGE_TYPE = 'translatePage'

export const SAOL_REQUEST_INFLECTION_TYPE = 'inflection'


const backendBaseUrl = '/queryWord/'

// the constants for analyzing xml file (Folk lexikon lookup results)
const KEY_BEFOREXML = "2600011424"
const OFFSET_BEFOREXML = 13
const OFFSET_AFTERXML = 10

// the constants for analyzing suggestion response
const KEY_BEFOREARR = '["com.google.gwt'
const OFFSET_AFTERARR = 5

// action creators for choosing data source (folk lexikon or ms api)
export const selectDataSource = dataSource => ({
    type: SELECT_DATA_SOURCE,
    dataSource
})

export const lookupKey = (dataSource, requestType, keyWord, srcLang, toLang) => ({
    type: LOOKUP_KEY,
    dataSource,
    requestType,
    keyWord,
    srcLang,
    toLang
})

export const requestDetails = (dataSource, requestType, keyWord) => ({
    type: REQUEST_DETAILS,
    dataSource,
    requestType,
    keyWord
})

export const receiveDetails = (dataSource, requestType, keyWord, details, srcLang, toLang) => ({
    type: RECEIVE_DETAILS,
    dataSource,
    requestType,
    keyWord,
    details,
    srcLang,
    toLang
})

// in case of failed request, use this action to mark 'isFetching' as false
export const requestFailed = (dataSource, requestType, keyWord) => ({
    type: REQUEST_FAILURE,
    dataSource,
    requestType,
    keyWord
})

// here we mark the current request status. If any failure, we can use the status to show the alert
export const markCurrentRequestStatus = (dataSource, code) => ({
    type: MARK_REQUEST_STATUS,
    dataSource,
    code
})

export const setNativeLanguage = (lang) => ({
    type: SET_NATIVE_LANGUAGE,
    lang
})

export const setUiLanguage = (lang) => ({
    type: SET_UI_LANGUAGE,
    lang
})

export const setTextTranslateEngine = (engine) => ({
    type: SET_TEXT_TRANSLATE_ENGINE,
    engine
})

// when changing native language, we have to clean up the ms api history
export const cleanHistoryData = (dataSource) => ({
    type: CLEAN_HISTORY_DATA,
    dataSource
})

export const showRequestFailure = (isShown) => ({
    type: SHOW_REQUEST_FAILURE,
    isShown
})

export const showFuzzyDialog = (dataSource, requestType, keyWord, isShown) => ({
    type: SHOW_FUZZY_DIALOG,
    dataSource,
    requestType,
    keyWord
})

function fetchDetails(dataSource, requestType, keyWord, srcLang, toLang) {
    return dispatch => {
        dispatch(requestDetails(dataSource, requestType, keyWord))

        switch (dataSource) {
            case DATA_SOURCE_FOLKLEX:
                fetchDataFromFolkLexikon(dispatch, dataSource, requestType, keyWord)
                break
            case DATA_SOURCE_MSAPI:
                fetchDataFromMsApi(dispatch, dataSource, requestType, keyWord, srcLang, toLang)
                break
            case DATA_SOURCE_GAPI:
                fetchDataFromGoogleApi(dispatch, dataSource, requestType, keyWord, srcLang, toLang)
                break
            case DATA_SOURCE_SAOL:
                fetchDataFromSaol(dispatch, dataSource, requestType, keyWord)
                break
            default:
                break
        }
    }
}

function fetchDataFromMsApi(dispatch, dataSource, requestType, keyWord, srcLang, toLang) {
    // here we handle both translate and translatePage request types
    const backendUrl = backendBaseUrl + requestType.replace('Page', '') + '/' + srcLang + '/' + toLang + '/' + keyWord
    fetchData(backendUrl, dispatch, dataSource, requestType, keyWord, srcLang, toLang)
}

function fetchDataFromGoogleApi(dispatch, dataSource, requestType, keyWord, srcLang, toLang) {
    const backendUrl = backendBaseUrl + requestType + '/' + srcLang + '/' + toLang + '/' + keyWord
    fetchData(backendUrl, dispatch, dataSource, requestType, keyWord, srcLang, toLang)
}

function fetchDataFromFolkLexikon(dispatch, dataSource, requestType, keyWord) {
    const backendUrl = backendBaseUrl + requestType + '/' + keyWord
    fetchData(backendUrl, dispatch, dataSource, requestType, keyWord)
}

function fetchDataFromSaol(dispatch, dataSource, requestType, keyWord) {
    const backendUrl = backendBaseUrl + requestType + '/' + keyWord
    fetchData(backendUrl, dispatch, dataSource, requestType, keyWord)
}

function fetchData(fetchUrl, dispatch, dataSource, requestType, keyWord, srcLang, toLang) {
    timeoutableFetch(fetchUrl)
        .then(response => dataSource === DATA_SOURCE_MSAPI ? response.json() : response.text())
        .then(contents => {
            console.log('get contents')
            console.log(contents)
            dispatch(markCurrentRequestStatus(dataSource, 200))    // mark request status
            let details
            if (dataSource === DATA_SOURCE_FOLKLEX) {
                details = analyzeFolketResponseText(contents, requestType, keyWord)
            } else if (dataSource === DATA_SOURCE_MSAPI || dataSource === DATA_SOURCE_GAPI) {
                details = contents
            } else {
                details = analyzeSaolResponseText(contents)
            }
            dispatch(receiveDetails(dataSource, requestType, keyWord, details, srcLang, toLang))
        })
        .catch(error => {
            console.log('Network error: ' + error.message)
            let code
            switch (error.message) {
                case 'Failed to fetch':
                    code = 400
                    break
                case 'Time out':
                    code = 408
                    break
                default:
                    if (error.message.startsWith('Unexpected token'))
                        code = 404
                    else
                        code = 400
            }
            dispatch(markCurrentRequestStatus(dataSource, code))
            dispatch(requestFailed(dataSource, requestType, keyWord))
        })
}

function analyzeSaolResponseText(responseText) {
    if (responseText.indexOf('i SAOL gav inga svar') !== -1) {
        return ''
    }

    // the ordformer information is a table in the html string
    const tableStart = responseText.lastIndexOf('<table')
    const tableEnd = responseText.lastIndexOf('table>')
    if (tableStart === -1 || tableEnd === -1) {
        return ''
    }

    let tableString = responseText.substring(tableStart, tableEnd + 6)
    return tableString
}

function analyzeFolketResponseText(responseText, requestType, keyWord) {
    // NOTE: need to check the range of the index here
    if (requestType === REQUEST_WORD_TYPE) {
        if (responseText.indexOf('<word') !== -1) {    // it is a "real" response: Folket found the word
            let subStart = responseText.indexOf(KEY_BEFOREXML) + OFFSET_BEFOREXML
            let subEnd = responseText.length - OFFSET_AFTERXML - keyWord.length
            let xmlString = responseText.substring(subStart, subEnd)

            //console.log(responseText)

            // Remove some illegal char
            xmlString = xmlString.replace(/\\n/g, "")
            xmlString = xmlString.replace(/\\"/g, "\"")
            xmlString = xmlString.replace(/>","</g, "><")
            xmlString = xmlString.replace(/&#39;/g, "'")
            xmlString = xmlString.replace(/&quot;/g, "")

            xmlString = xmlString.replace(/&amp;quot;/g, "'")
            xmlString = xmlString.replace(/&amp;/g, "")
            xmlString = xmlString.replace(/&quot;/g, "")
            xmlString = xmlString.replace(/#39;/g, "'")

            // Make a complete xml file for further analysis
            xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><result>" + xmlString + "</result>";

            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(xmlString,"text/xml")

            return xmlDoc
        } else {    // cannot find the word in Folket. In this case, it will return a list of suggestions
            let arrString = responseText.substring(responseText.indexOf('["se.algoritmica'), responseText.length - OFFSET_AFTERARR)
            let arrSuggestions = JSON.parse(arrString)

            return arrSuggestions.filter(suggestion => !suggestion.startsWith('se.algoritmica')
                && !suggestion.startsWith('[I/2970817851')
                && !suggestion.startsWith('[Ljava'))
                //&& suggestion !== keyWord)    // keep the original invalid input and use the item to show 'try native translation'
        }
    } else {
        // Folket will return the suggestions based on current input
        let arrString = responseText.substring(responseText.indexOf(KEY_BEFOREARR), responseText.length - OFFSET_AFTERARR)

        let arrSuggestions = JSON.parse(arrString)

        return arrSuggestions.filter(suggestion => !suggestion.startsWith('com.google')
                                                && !suggestion.startsWith('java.util')
                                                && !suggestion.startsWith('se.algoritmica')
                                                && !suggestion.startsWith('<img'))
    }
}

function shouldFetchDetails(dataSource, requestType, state) {
    const {isFetching, history, presentIndex} = state.dataBySource[dataSource][requestType]

    const keyWord = history[presentIndex].keyWord
    const details = history[presentIndex].details

    return !isFetching && keyWord !== null && keyWord !== '' && isObjectEmpty(details)
}

export function fetchDetailsIfNeeded(dataSource, requestType, keyWord, srcLang, desLang) {
    return (dispatch, getState) => {
        if (shouldFetchDetails(dataSource, requestType, getState())){
            return dispatch(fetchDetails(dataSource, requestType, keyWord, srcLang, desLang))
        }
    }
}
