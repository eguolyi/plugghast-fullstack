public class FragmentDict extends Fragment implements OnClickListener {

    public class OfflineDictDatabaseHelper extends SQLiteOpenHelper {

        /**
         * Table Schema:
         * 'Words' (ID NUMERIC,Native TEXT,Class TEXT,Phonetic TEXT,Comment TEXT);
         * 'Paradigms' (ID NUMERIC,Inflection TEXT);
         * 'Translations' (ID NUMERIC,Translation TEXT,Comment TEXT);
         * 'Examples' (ID NUMERIC,Native TEXT,Translation TEXT);
         * 'Idioms' (ID NUMERIC,Native TEXT,Translation TEXT);
         * 'Compounds' (ID NUMERIC,Native TEXT,Translation TEXT);
         * 'Definitions' (ID NUMERIC,Native TEXT,Translation TEXT);
         */
        static final String TABLE_WORDS = "Words";
        static final String TABLE_TRANS = "Translations";
        static final String TABLE_PARAD = "Paradigms";
        static final String TABLE_DEFIN = "Definitions";
        static final String TABLE_EXAMP = "Examples";
        static final String TABLE_COMPO = "Compounds";
        static final String TABLE_IDIOM = "Idioms";

        static final String SELECT_FROM_WORDS_TABLE = "select * from Words where Native=";
        static final String SELECT_FROM_PARAD_TABLE = "select * from Paradigms where ID=%d";
        static final String SELECT_FROM_TRANS_TABLE = "select * from Translations where ID=%d";
        static final String SELECT_FROM_EXAMP_TABLE = "select * from Examples where ID=%d";
        static final String SELECT_FROM_IDIOM_TABLE = "select * from Idioms where ID=%d";
        static final String SELECT_FROM_COMPO_TABLE = "select * from Compounds where ID=%d";
        static final String SELECT_FROM_DEFIN_TABLE = "select * from Definitions where ID=%d";

        public ArrayList<HashMap<String, String>> getOfflineTrans(String keyWord) {
            ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();



            return data;
        }

        public OfflineDictDatabaseHelper(Context context, String name, int version) {
            super(context, name, null, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

    }

    private OfflineDictDatabaseHelper offlineDB;
    private String OFFLINEDBNAME = "offlinedict.db3";
    public boolean useOfflineDB = false;
    private String[] offlineSuggestion = null;

    private TextView tv;
    enum ConnType { LOOKUP, SUGGESTION };

    private AutoCompleteTextView edit_input;
    private Lock bufferLock = new ReentrantLock();
    private String ReplyStr = "";
    private String SuggStr = "";
    private String tmp_input; // temporary input in the EditText. Will use it to fetch completion suggestion

    private ArrayList<HashMap<String, String>> menuItems;
    private ListView main_list;

    private boolean connected = true;

    SharedPreferences preferences;
    SharedPreferences.Editor pEditor;

    // the constants for analyzing xml file (lookup results)
    static final String KEY_WORD = "word";
    static final String KEY_TRANSLATE = "translation";
    static final String KEY_WORDTRANSLATE = "word_translation";
    static final String KEY_WORDTYPE = "word_type";
    static final String KEY_PRONUCIATION = "word_pronuciation";
    static final String KEY_VALUE = "value";
    static final String KEY_TYPE = "type";
    static final String KEY_TRANSCOMMENT = "trans_comment";
    static final String KEY_SOUNDFILE = "sound_file";
    static final String KEY_INSERTDATE = "insert_date";
    static final String KEY_XML = "xml";
    static final String KEY_ID = "word_id";
    static final String KEY_MODE = "display_mode";
    static final String KEY_SUCCESS = ",2,1,0,1,[";
    static final String KEY_BEFOREXML = "2600011424";
    static final String KEY_BEFORESUG = "2026105502";
    static final int OFFSET_BEFOREXML = 13;
    static final int OFFSET_BEFORESUG = 13;

    static final String ONLINE_DICT_NO_LOOKUP = "//OK[2,0,0,1,0,";
    static final String ONLINE_DICT_NO_SUGGESTION = "//OK[0,2,0,0,1,";

    private ProgressDialog m_ProgressDialog = null;

    ArrayList<String> Proposal = new ArrayList<String>();
    //private final String

    /*********************************************************************
     * following types and variables will be used for translate word translation to current native language
     */
    enum NativeLanguage { CN, TH, AR, NONE };
    static NativeLanguage curNativeLang = NativeLanguage.NONE;

    enum InputType { WORD, SENTENCE };
    static InputType curInputType = InputType.WORD;
    /*********************************************************************/

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch ( msg.what ) {
                case 0x123:			// look up a word
                    if (connected)
                        analyzeResponse();
                    else {
                        Context ct = FragmentDict.this.getActivity();
                        if ( ct!= null ) {
                            //Toast.makeText(ct, "Can not connect to server...", Toast.LENGTH_LONG).show();
                            String strAlert = "It seems there is no network connection at the moment, please click the button # on the title bar to switch to the Offline Dictionary :)";
                            Spannable text = new SpannableString(strAlert);
                            int space = strAlert.indexOf('#');
                            if (space > 0 && space < strAlert.length()-1) {
                                Drawable btnDict = getActivity().getResources().getDrawable(R.drawable.offline_dict_small);
                                btnDict.setBounds(0, 0, btnDict.getMinimumWidth(), btnDict.getMinimumHeight());
                                ImageSpan imgEnSpan = new ImageSpan(btnDict, ImageSpan.ALIGN_BASELINE);
                                text.setSpan(imgEnSpan, space, space+1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                            }
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(R.drawable.alert)
                                    .setTitle("No network connection...")
                                    .setMessage(text)
                                    .setPositiveButton("OK", null)
                                    .create().show();
                        }
                    }


                    break;
                case 0x124:			// when a user is typing, give word suggestion
                    if (connected)
                        analyzeSuggestion();
                    break;
                case 0x125:			// query offline database
                    DictAdapter adapter = new DictAdapter(getView().getContext(), menuItems, edit_input.getText().toString());
                    main_list.setAdapter(adapter);
                    break;
                case 0x126:			// give word suggestion in offline mode
                    analyzeOfflineSuggestion();
                    break;
                case 0x127:
                    Toast.makeText(getActivity(), "There is no such word in the dictionary :(", Toast.LENGTH_SHORT).show();
                    menuItems.clear();
                    main_list.invalidateViews();
                    break;
                case 0x128: // mandatory timer message for killing the progress bar
                    break;
            }

            if (m_ProgressDialog != null) {
                m_ProgressDialog.dismiss();
                m_ProgressDialog = null;
            }

        }
    };

    private void analyzeOfflineSuggestion() {
        bufferLock.lock();
        try {
            if (offlineSuggestion != null) {
                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(this.getActivity(),
                                android.R.layout.simple_dropdown_item_1line, offlineSuggestion);
                edit_input.setAdapter(adapter);
            }
        }
        finally {
            bufferLock.unlock();
        }


    }

    /**
     * analyze the suggestion information from the server (for autocomplete)
     */
    private void analyzeSuggestion() {
        int substart;
        int subend;
        String[] suggestions;
        //System.out.println("--------------analyzeSuggestion--------");
        bufferLock.lock();
        try {
            if (SuggStr.isEmpty() || !SuggStr.startsWith("//OK"))
                return;

            //System.out.println("in Suggest:" + SuggStr);
            substart = SuggStr.indexOf(KEY_BEFORESUG) + OFFSET_BEFORESUG;
            subend = SuggStr.length() - 7;

            // illegal index
            if (substart < 0 || substart > SuggStr.length()-1
                    || subend < 0 || subend > SuggStr.length()-1
                    || subend <= substart)
                return;

            //System.out.println(SuggStr);
            //System.out.println("start: " + substart + " end: " + subend);
            SuggStr = SuggStr.substring(substart, subend);

            SuggStr = SuggStr.replace("\",\"", ",");
            suggestions = SuggStr.split(",");
            //System.out.println("---------------befor crash: " + SuggStr);
            //System.out.println(suggestions.length);

            SuggStr = "";

        }
        finally {
            bufferLock.unlock();
        }

        Context ct = this.getActivity();
        if (ct != null) {
            ArrayAdapter<String> adapter =
                    new ArrayAdapter<String>(ct, android.R.layout.simple_dropdown_item_1line, suggestions);
            edit_input.setAdapter(adapter);
        }

        //edit_input.showDropDown();
    }

    /**
     * analyze the query response
     */
    private void analyzeResponse() {

        int substart;
        int subend;

        bufferLock.lock();
        try {
            //
            if (ReplyStr.contains("//OK[3,")) {
                //Context ct = this.getActivity();
                //if ( ct != null )
                //	Toast.makeText(ct, "Can NOT find the word!", Toast.LENGTH_SHORT).show();

                return;
            }

            if (ReplyStr.isEmpty() || !ReplyStr.startsWith("//OK"))
                return;

            substart = ReplyStr.indexOf(KEY_BEFOREXML) + OFFSET_BEFOREXML;
            subend = ReplyStr.length() - 10 - edit_input.length();

            // illegal index
            if (substart < 0 || substart > ReplyStr.length()-1
                    || subend < 0 || subend > ReplyStr.length()-1
                    || subend <= substart)
                return;

            ReplyStr = ReplyStr.substring(substart, subend);

            // Remove some illegal char
            ReplyStr = ReplyStr.replace("\\n", "");
            ReplyStr = ReplyStr.replace("\\\"", "\"");
            ReplyStr = ReplyStr.replace(">\",\"<", "><");
            ReplyStr = ReplyStr.replace("&#39;", "\' ");
            ReplyStr = ReplyStr.replace("&quot;", "");
            //

            ReplyStr = ReplyStr.replace("&amp;quot;", "\'");
            ReplyStr = ReplyStr.replace("&amp;", "");
            ReplyStr = ReplyStr.replace("&quot;", "");
            ReplyStr = ReplyStr.replace("#39;", "\' ");

            // Make a complete xml file for further analysis
            ReplyStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><result>"
                    + ReplyStr;
            ReplyStr = ReplyStr + "</result>\n";

            parseXML(ReplyStr);

            ReplyStr = "";
            //
        } catch (Throwable t) {
            t.printStackTrace();
        }
        finally {
            bufferLock.unlock();
        }
        //


        return;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dict, container, false);
        v.findViewById(R.id.btn_ai).setOnClickListener(this);
        v.findViewById(R.id.btn_ao).setOnClickListener(this);
        v.findViewById(R.id.btn_ou).setOnClickListener(this);
        v.findViewById(R.id.btn_lookup).setOnClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        preferences = getActivity().getSharedPreferences("currentlookup", Context.MODE_PRIVATE);
        pEditor = preferences.edit();

        // set the right Dictionary mode
        String strDictMode = preferences.getString("DictMode", null);
        if (strDictMode != null) {
            if (strDictMode.equals("Dictionary"))
                useOfflineDB = false;
            else
                useOfflineDB = true;
        }

        // set native language
        String strNative = preferences.getString("NativeLang", null);
        if (strNative != null) {
            if (strNative.equals("CN")) {
                curNativeLang = NativeLanguage.CN;
            } else if (strNative.equals("TH")) {
                curNativeLang = NativeLanguage.TH;
            } else if (strNative.equals("AR")) {
                curNativeLang = NativeLanguage.AR;
            } else {
                curNativeLang = NativeLanguage.NONE;
            }
        } else {
            curNativeLang = NativeLanguage.NONE;
        }

        tv = (TextView) getView().findViewById(R.id.titleTv);
        tv.setText(!useOfflineDB ? "Dictionary(online)" :"Dictionary(offline)");

        Drawable dictMode;
        ImageButton ib = (ImageButton) getView().findViewById(R.id.btn_titlebar);
        if (useOfflineDB) {
            dictMode = getResources().getDrawable(R.drawable.dict_offline_mode);
            ib.setImageResource(R.drawable.online_dict);
        } else {
            dictMode = getResources().getDrawable(R.drawable.dict_online_mode);
            ib.setImageResource(R.drawable.offline_dict);
        }

        dictMode.setBounds(0, 0, dictMode.getMinimumWidth(), dictMode.getMinimumHeight());
        tv.setCompoundDrawables(dictMode, null, null, null);
        tv.setCompoundDrawablePadding(3);

        ib.setOnClickListener(this);

        ImageButton ib2 = (ImageButton) getView().findViewById(R.id.btn_titlebar2);
        ib2.setImageResource(R.drawable.set_native);
        ib2.setOnClickListener(this);

        edit_input = (AutoCompleteTextView) getView().findViewById(R.id.edit_lookup);
        edit_input.setThreshold(3);

        edit_input.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                tmp_input = arg0.toString();
                //System.out.println("onTextChange: " + tmp_input);

                if (tmp_input.length() >= 3) {

                    new Thread() {
                        @Override
                        public void run() {
                            if (!useOfflineDB) {
                                if (queryDatabase(ConnType.SUGGESTION)) {
                                    handler.sendEmptyMessage(0x124);
                                }
                            } else {
                                if (queryOfflineDatabase(ConnType.SUGGESTION)) {
                                    handler.sendEmptyMessage(0x126);
                                }
                            }

                        }
                    }.start();

                }
            }

        });

        // when an item in dropdown list is selected, this will be invoked
        edit_input.setOnItemClickListener(new OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                //edit_input.dismissDropDown();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_input.getWindowToken(), 0);

                menuItems.clear();
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        if (!useOfflineDB) {
                            if (queryDatabase(ConnType.LOOKUP)) {
                                handler.sendEmptyMessage(0x123);
                            } else {
                                handler.sendEmptyMessage(0x127);
                            }

                        } else {
                            if (queryOfflineDatabase(ConnType.LOOKUP)) {
                                handler.sendEmptyMessage(0x125);
                            } else {
                                handler.sendEmptyMessage(0x127);
                            }

                        }

                    }
                }.start();
                m_ProgressDialog = ProgressDialog.show(getActivity(), "", "Looking up ...", true);

                // timer for killing the progress bar if they are still there due to some reason
                new Timer().schedule(new TimerTask() {

                    @Override
                    public void run() {
                        handler.sendEmptyMessage(0x128);
                    }

                }, 5000);

            }

        });

        //System.out.println("-----onactivitycreated---------: " + edit_input.getText().toString() );

        main_list = (ListView) getView().findViewById(R.id.lview_lookup);
        menuItems = new ArrayList<HashMap<String, String>>();

        // initialize offline database
        copyOfflineDBToPhone();
        offlineDB = new OfflineDictDatabaseHelper(getActivity(), OFFLINEDBNAME, 1);

        recoverLookup();

    }

    /**
     * recover the previous lookup results in the list view
     */
    private void recoverLookup() {
        String previousLookup = preferences.getString("lookup", null);
        if ( previousLookup != null ) {
            if (!previousLookup.isEmpty()) {
                edit_input.setText(previousLookup);
                View btnLookup = getView().findViewById(R.id.btn_lookup);
                onClick(btnLookup);
            }
        } else {
            edit_input.setText(getActivity().getResources().getString(R.string.welcome_word));
            View btnLookup = getView().findViewById(R.id.btn_lookup);
            onClick(btnLookup);
        }
    }

    /**
     * query the database (online) according the connection type
     * @param type: connection type
     * @return: raw data
     */
    private boolean queryDatabase(ConnType type)
    {
        URLConnection connection;
        BufferedReader in = null;
        OutputStream output = null;
        String content = null;
        String conn_url = null;
        connected = true;

        bufferLock.lock();
        try {
            switch (type){
                case LOOKUP:
                    content = "7|0|6|http://folkets-lexikon.csc.kth.se/folkets/folkets/|1F6DF5ACEAE7CE88AACB1E5E4208A6EC|"
                            + "se.algoritmica.folkets.client.LookUpService|lookUpWord|"
                            + "se.algoritmica.folkets.client.LookUpRequest/1089007912|"
                            + edit_input.getText() + "|1|2|3|4|1|5|5|1|0|0|6|\n";
                    conn_url = "http://folkets-lexikon.csc.kth.se/folkets/folkets/lookupword";

                    break;

                case SUGGESTION:
                    content = "7|0|7|http://folkets-lexikon.csc.kth.se/folkets/folkets/|72408650102EFF3C0092D16FF6C6E52F|"
                            + "se.algoritmica.folkets.client.ItemSuggestService|getSuggestions|"
                            + "se.algoritmica.folkets.client.ProposalRequest/3613917143|"
                            + "com.google.gwt.user.client.ui.SuggestOracle$Request/3707347745|"
                            + edit_input.getText() + "|1|2|3|4|1|5|5|1|6|5|7|\n";
                    conn_url = "http://folkets-lexikon.csc.kth.se/folkets/folkets/generatecompletion";

                    break;
                default:
                    break;
            }

            connection = new URL(conn_url).openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            connection.setRequestProperty("Content-Type",
                    "text/x-gwt-rpc; charset=utf-8");
            connection.setRequestProperty("X-GWT-Permutation",
                    "D88EC72BE0FA10F91FDC911C7757B7F3");
            connection.setRequestProperty("X-GWT-Module-Base",
                    "http://folkets-lexikon.csc.kth.se/folkets/folkets/");

            connection.setDoOutput(true);
            connection.setDoInput(true);

            // Write body
            output = connection.getOutputStream();
            output.write(content.getBytes());
            output.flush();


            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), "UTF-8"));

            String inputLine;

            switch (type)
            {
                case LOOKUP:
                    ReplyStr = "";
                    while ((inputLine = in.readLine()) != null)
                        ReplyStr = ReplyStr + inputLine;

                    //System.out.println("query database " + type + ":\n" + ReplyStr);
                    if (ReplyStr.startsWith(ONLINE_DICT_NO_LOOKUP)) {
                        return false;
                    }

                    break;
                case SUGGESTION:
                    SuggStr = "";
                    while ((inputLine = in.readLine()) != null)
                        SuggStr = SuggStr + inputLine;

                    //System.out.println("query database " + type + ":\n" + SuggStr);
                    if (SuggStr.startsWith(ONLINE_DICT_NO_SUGGESTION)) {
                        return false;
                    }

                    break;
                default:
                    break;

            }

            return true;
        }catch (Exception e) {

            connected = false; // it seems no network connection at the moment

            e.printStackTrace();
        }
        finally
        {
            bufferLock.unlock();
            try
            {
                if (output != null){
                    output.close();
                }
                if (in != null){
                    in.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }

        }

        return true;

    }

    /**
     * Try to parse XML file in a more simple way
     * @param str
     */
    private void parseXML(String str) {
        //
        XMLParser parser = new XMLParser();
        Document doc;
        NodeList nl;

        try {
            //
            doc = parser.getDomElement(str);
            nl = doc.getElementsByTagName("word");
            //
        } catch (Throwable t) {
            //
            t.printStackTrace();
            //Toast.makeText(this.getActivity(), "XML Parse error..", Toast.LENGTH_LONG).show();
            return;
        }
        //
        // looping through all item nodes <word>
        //
        for (int i = 0; i < nl.getLength(); i++) {
            //
            Element e_word = (Element) nl.item(i);

            // Only display sv --> en translation results in the main view
            if (e_word.getAttribute("lang").toString().equals("sv")) {
                String word_in = "";
                String word_type = "";
                String word_comment = "";
                String translation = "";
                String trans_comment = "";
                String phonetic = "";
                String inflection = "";
                String synonym = "";
                String example = "";
                String definition = "";
                String soundfile = "";
                String use = "";
                String compound = "";
                String idiom = "";

                word_in = e_word.getAttribute("value").toString();
                word_type = getWordType(e_word.getAttribute("class").toString());
                word_comment = e_word.getAttribute("comment").toString();

                NodeList word_nodes = e_word.getChildNodes();
                // list all the child nodes and record the results
                for (int j = 0; j < word_nodes.getLength(); j++) {

                    //System.out.println("current in node " + word_nodes.item(j).getNodeName());
                    // child node of 'translation'
                    if (word_nodes.item(j).getNodeName().equals("translation")) {
                        Element e_tr = (Element) word_nodes.item(j);
                        if (e_tr != null){
                            String sep = translation.isEmpty() ? "" : ", ";
                            translation = translation + sep + e_tr.getAttribute("value").toString();
                            trans_comment = e_tr.getAttribute("comment").toString();
                        }
                    }

                    // child node of 'use'
                    if (word_nodes.item(j).getNodeName().equals("use")) {
                        Element e_use = (Element) word_nodes.item(j);
                        if (e_use != null) {
                            use += e_use.getAttribute("value").toString();
                        }
                    }

                    // child node of 'phonetic'
                    if (word_nodes.item(j).getNodeName().equals("phonetic")) {
                        Element e_ph = (Element) word_nodes.item(j);
                        if (e_ph != null) {
                            phonetic += e_ph.getAttribute("value").toString();
                            soundfile = e_ph.getAttribute("soundFile").toString();
                        }

                    }

                    // get the 'inflection' information from the child node of 'paradigm'
                    if (word_nodes.item(j).getNodeName().equals("paradigm")) {
                        NodeList parad = word_nodes.item(j).getChildNodes();
                        for (int p = 0; p < parad.getLength(); p++) {
                            Element e_if = (Element) parad.item(p);
                            inflection = inflection + e_if.getAttribute("value").toString()
                                    + ((p == parad.getLength()-1) ? "" : ", ");
                        }
                    }

                    // child node of 'synonym'
                    if (word_nodes.item(j).getNodeName().equals("synonym")) {
                        Element e_sy = (Element) word_nodes.item(j);
                        if (e_sy != null) {
                            String sep = synonym.isEmpty() ? "" : ", ";
                            synonym = synonym + sep + e_sy.getAttribute("value").toString();
                        }

                    }

                    // child node of 'example'
                    if (word_nodes.item(j).getNodeName().equals("example")) {
                        Element e_ex = (Element) word_nodes.item(j);
                        if (e_ex != null){
                            String sep = example.isEmpty() ? "" : ",\n   ";
                            example = example + sep + e_ex.getAttribute("value").toString();

                            NodeList parad = e_ex.getChildNodes();
                            for (int p = 0; p < parad.getLength(); p++) {
                                if (parad.item(p).getNodeName().equals("translation")) {
                                    String ex_trans = ((Element)parad.item(p)).getAttribute("value").toString();
                                    example += " (" + ex_trans + ")";
                                }
                            }
                        }
                    }

                    // child node of 'definition'
                    if (word_nodes.item(j).getNodeName().equals("definition")) {
                        Element e_de = (Element) word_nodes.item(j);
                        if (e_de != null) {
                            definition += e_de.getAttribute("value").toString();

                            NodeList parad = e_de.getChildNodes();
                            for (int p = 0; p < parad.getLength(); p++) {
                                if (parad.item(p).getNodeName().equals("translation")) {
                                    String de_trans = ((Element)parad.item(p)).getAttribute("value").toString();
                                    definition += " (" + de_trans + ")";
                                }
                            }
                        }
                    }

                    // child node of 'compound'
                    if (word_nodes.item(j).getNodeName().equals("compound")) {
                        Element e_cp = (Element) word_nodes.item(j);
                        if (e_cp != null) {
                            String sep = compound.isEmpty() ? "" : ",\n   ";
                            compound = compound + sep + e_cp.getAttribute("value").toString();

                            NodeList parad = e_cp.getChildNodes();
                            for (int p = 0; p < parad.getLength(); p++) {
                                if (parad.item(p).getNodeName().equals("translation")) {
                                    String cp_trans = ((Element)parad.item(p)).getAttribute("value").toString();
                                    compound += " (" + cp_trans + ")";
                                }
                            }
                        }
                    }

                    // child node of 'idiom'
                    if (word_nodes.item(j).getNodeName().equals("idiom")) {
                        Element e_idm = (Element) word_nodes.item(j);
                        if (e_idm != null) {
                            String sep = idiom.isEmpty() ? "" : ",\n   ";
                            idiom = idiom + sep + e_idm.getAttribute("value").toString();

                            NodeList parad = e_idm.getChildNodes();
                            for (int p = 0; p < parad.getLength(); p++) {
                                if (parad.item(p).getNodeName().equals("translation")) {
                                    String idm_trans = ((Element)parad.item(p)).getAttribute("value").toString();
                                    idiom += " (" + idm_trans + ")";
                                }
                            }
                        }
                    }
                }

                word_type = word_type + (word_comment.isEmpty()? "" : (" (" + word_comment + ") "));
                //translation = translation + (trans_comment.isEmpty()? "" : (" (" + trans_comment + ") "));
                trans_comment = trans_comment.isEmpty() ? "" : (" (" + trans_comment + ")");
                if ( phonetic.isEmpty() )
                    phonetic = "N/A";

                use = use.isEmpty() ? "" : ("Use: " + use + "\n");
                inflection = inflection.isEmpty()? "" : ("Inflections: " + inflection + "\n");
                synonym = synonym.isEmpty() ? "" : ("Synonyms: " + synonym + "\n");
                definition = definition.isEmpty() ? "" : ("Definition: " + definition + "\n");
                example = example.isEmpty() ? "" : ("Examples: " + example + "\n");
                compound = compound.isEmpty() ? "" : ("Compounds: " + compound + "\n");
                idiom = idiom.isEmpty() ? "" : ("Idiom: " + idiom);

                String tmp = use + inflection + definition + example + synonym + compound + idiom;

                HashMap<String, String> map = new HashMap<String, String>();
                map.put(KEY_WORD, word_in + " ");
                map.put(KEY_WORDTYPE, word_type);
                map.put(KEY_WORDTRANSLATE, translation + trans_comment);
                map.put(KEY_TRANSCOMMENT, trans_comment);
                map.put(KEY_PRONUCIATION, "Pronuciation: [" + phonetic + "]");
                map.put(KEY_TRANSLATE, tmp);
                map.put(KEY_SOUNDFILE, soundfile);
                menuItems.add(map);
            }
        }
        // main_list.invalidateViews();

        DictAdapter adapter = new DictAdapter(getView().getContext(), menuItems, edit_input.getText().toString());

        main_list.setAdapter(adapter);

        //
        return;
    }

    /**
     * get the type of the word from its class value in xml file
     * @param word_class
     * @return
     */
    private String getWordType(String word_class)
    {
        if (word_class.equals("nn"))
            return "n.";

        if (word_class.equals("vb"))
            return "v.";

        if (word_class.equals("jj"))
            return "adj.";

        if (word_class.equals("in"))
            return "interj.";

        if (word_class.equals("pn"))
            return "pron.";

        if (word_class.equals("ab"))
            return "adv.";

        if (word_class.equals("kn"))
            return "conj.";

        if (word_class.equals("pp"))
            return "prep.";

        if (word_class.equals("rg"))
            return "num.";

        return "";
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_ao:
                edit_input.append(getResources().getString(R.string.ao));
                break;
            case R.id.btn_ai:
                edit_input.append(getResources().getString(R.string.ai));
                break;
            case R.id.btn_ou:
                edit_input.append(getResources().getString(R.string.ou));
                break;
            case R.id.btn_lookup:
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_input.getWindowToken(), 0);
                menuItems.clear();
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        if (!useOfflineDB) {
                            if (queryDatabase(ConnType.LOOKUP)) {
                                handler.sendEmptyMessage(0x123);
                            } else {
                                handler.sendEmptyMessage(0x127);
                            }

                        } else {
                            if (queryOfflineDatabase(ConnType.LOOKUP)) {
                                handler.sendEmptyMessage(0x125);
                            } else {
                                handler.sendEmptyMessage(0x127);
                            }
                        }
                    }
                }.start();

                m_ProgressDialog = ProgressDialog.show(getActivity(), "", "Looking up ...", true);

                // timer for killing the progress bar if they are still there due to some reason
                new Timer().schedule(new TimerTask() {

                    @Override
                    public void run() {
                        handler.sendEmptyMessage(0x128);
                    }

                }, 5000);

                break;
            case R.id.btn_titlebar:
                useOfflineDB = !useOfflineDB;
                String strTitle = !useOfflineDB ? "Dictionary(online)" :"Dictionary(offline)";
                tv.setText(strTitle);
                Drawable dictMode;
                ImageButton ib = (ImageButton) getView().findViewById(R.id.btn_titlebar);
                if (useOfflineDB) {
                    dictMode = getResources().getDrawable(R.drawable.dict_offline_mode);
                    ib.setImageResource(R.drawable.online_dict);
                }
                else {
                    dictMode = getResources().getDrawable(R.drawable.dict_online_mode);
                    ib.setImageResource(R.drawable.offline_dict);
                }

                dictMode.setBounds(0, 0, dictMode.getMinimumWidth(), dictMode.getMinimumHeight());
                tv.setCompoundDrawables(dictMode, null, null, null);
                tv.setCompoundDrawablePadding(3);
                Toast.makeText(getActivity(), "Use " + strTitle, Toast.LENGTH_SHORT).show();

                // clean current content of the list
                menuItems.clear();
                main_list.invalidateViews();
                break;
            case R.id.btn_titlebar2:
                LinearLayout ll = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.set_nativelang_dialog, null);
                RadioGroup rgSetNative = (RadioGroup) ll.findViewById(R.id.rg_set_native_lang);
                RadioButton rbCN = (RadioButton) ll.findViewById(R.id.rb_set_native_cn);
                RadioButton rbTH = (RadioButton) ll.findViewById(R.id.rb_set_native_th);
                RadioButton rbAR = (RadioButton) ll.findViewById(R.id.rb_set_native_ar);
                RadioButton rbNONE = (RadioButton) ll.findViewById(R.id.rb_set_native_none);
                switch (curNativeLang) {
                    case CN:
                        rbCN.setChecked(true);
                        break;
                    case NONE:
                        rbNONE.setChecked(true);
                        break;
                    case TH:
                        rbTH.setChecked(true);
                        break;
                    case AR:
                        rbAR.setChecked(true);
                        break;
                    default:
                        break;

                }
                rgSetNative.setOnCheckedChangeListener(new OnCheckedChangeListener()
                {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch(checkedId) {
                            case R.id.rb_set_native_cn:
                                curNativeLang = NativeLanguage.CN;
                                break;
                            case R.id.rb_set_native_th:
                                curNativeLang = NativeLanguage.TH;
                                break;
                            case R.id.rb_set_native_ar:
                                curNativeLang = NativeLanguage.AR;
                                break;
                            case R.id.rb_set_native_none:
                                curNativeLang = NativeLanguage.NONE;
                                break;
                        }

                    }

                });
                new AlertDialog.Builder(getActivity())
                        .setIcon(R.drawable.app_logo_small)
                        .setTitle("Specify your mother tongue")
                        .setView(ll)
                        .setPositiveButton("OK", null)
                        .create().show();

                break;
            default:
                break;
        }
    }

    /**
     * Query offline database when network is unavailable
     * @param type
     */
    private boolean queryOfflineDatabase(ConnType type) {

        bufferLock.lock();
        try {
            switch (type){
                case LOOKUP:
                    return getOfflineTrans(edit_input.getText().toString().trim());

                case SUGGESTION:
                    return getOfflineSuggestion(edit_input.getText().toString().trim());

                default:
                    break;
            }

            return true;

        }catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            bufferLock.unlock();
        }
        return false;
    }

    /**
     * when the user is typing the word, give the spell suggestion from offline database
     * @param keyWord
     */
    private boolean getOfflineSuggestion(String tmpInput) {
        SQLiteDatabase db = offlineDB.getReadableDatabase();
        ArrayList<String> listSuggestion = new ArrayList<String>();

        // search Words table to see if there is such a word whose name starts with "tmpInput"
        getOfflineSuggestionFromOneTable(db, "Words", "Native", tmpInput, listSuggestion);

        // search Paradigms table to see if there is such a inflection whose name starts with "tmpInput"
        getOfflineSuggestionFromOneTable(db, "Paradigms", "Inflection", tmpInput, listSuggestion);

        // search Compounds table
        getOfflineSuggestionFromOneTable(db, "Compounds", "Native", tmpInput, listSuggestion);

        if (listSuggestion.size() == 0) {
            return false;
        }

        removeDuplicatedElement(listSuggestion);

        bufferLock.lock();
        try {
            offlineSuggestion = (String[]) listSuggestion.toArray(new String[listSuggestion.size()]);

        }catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            bufferLock.unlock();
        }

        return true;
    }

    private void getOfflineSuggestionFromOneTable(SQLiteDatabase db,
                                                  String tblName, String colName, String tmpInput, ArrayList<String> list) {

        String strQuery = "select * from " + tblName + " where " + colName + " like \"" + tmpInput + "%\"";
        Cursor cursor = db.rawQuery(strQuery, null);

        while (cursor.moveToNext()) {
            //System.out.println(cursor.getString(1));
            list.add(cursor.getString(1));
        }
    }

    /**
     * Remove the duplicated items in a String array. The function
     * is used for handling offline suggestion results
     * @param list
     */
    private static void removeDuplicatedElement(ArrayList<String> list) {
        Set<String> set = new HashSet<String>();
        ArrayList<String> newList = new ArrayList<String>();
        for(Iterator<String> iter = list.iterator(); iter.hasNext(); ) {
            String element = iter.next();
            if (set.add(element))
                newList.add(element);
        }

        list.clear();
        list.addAll(newList);
    }

    /**
     * use the keyWord to search tables Words, paradigms and Compounds and get the related words' id
     * with these id, get full query results
     * @param keyWord
     */
    private boolean getOfflineTrans(String keyWord) {
        menuItems.clear();

        // the set of ids which match the query (with Set, the duplicated ones will be removed automatically)
        TreeSet<Integer> idSet = new TreeSet<Integer>();
        SQLiteDatabase db = offlineDB.getReadableDatabase();

        getOfflineTransFromOneTable(db, "Words", "Native", keyWord, idSet);
        getOfflineTransFromOneTable(db, "Paradigms", "Inflection", keyWord, idSet);
        getOfflineTransFromOneTable(db, "Compounds", "Native", keyWord, idSet);

        if (idSet.size() == 0) {
            return false;
        }

        Iterator<Integer> iter = idSet.iterator();
        while (iter.hasNext()) {
            int word_id = iter.next().intValue();
            String strIdQuery = String.format("select * from Words where ID=%d", word_id);
            Cursor cursor = db.rawQuery(strIdQuery, null);
            while (cursor.moveToNext()) { // actually, the cursor query only has one record (id the primary key)
                HashMap<String, String> map = new HashMap<String, String>();
                String word_in = cursor.getString(1);
                String word_type = getWordType(cursor.getString(2));
                String word_comment = cursor.getString(4);
                String phonetic = cursor.getString(3);

                String inflection = "";
                //int word_id = cursor.getInt(0);	// get the record id and we will use it to query more info from other tables
                String strQuery = String.format(OfflineDictDatabaseHelper.SELECT_FROM_PARAD_TABLE, word_id);
                Cursor cur = db.rawQuery(strQuery, null);
                while (cur.moveToNext()) {
                    String sep = inflection.isEmpty() ? "" : ", ";
                    inflection = inflection + sep + cur.getString(1);
                }

                String word_trans = getOneRecord(db, OfflineDictDatabaseHelper.SELECT_FROM_TRANS_TABLE, word_id, ", ");
                String word_example = getOneRecord(db, OfflineDictDatabaseHelper.SELECT_FROM_EXAMP_TABLE, word_id, ",\n   ");
                String word_idiom = getOneRecord(db, OfflineDictDatabaseHelper.SELECT_FROM_IDIOM_TABLE, word_id, ",\n   ");
                String word_compo = getOneRecord(db, OfflineDictDatabaseHelper.SELECT_FROM_COMPO_TABLE, word_id, ",\n   ");
                String word_defin = getOneRecord(db, OfflineDictDatabaseHelper.SELECT_FROM_DEFIN_TABLE, word_id, ", ");

                word_type = word_type + (word_comment.isEmpty()? "" : (" (" + word_comment + ") "));
                inflection = inflection.isEmpty()? "" : ("Inflections: " + inflection + "\n");
                word_defin = word_defin.isEmpty() ? "" : ("Definition: " + word_defin + "\n");
                word_example = word_example.isEmpty() ? "" : ("Examples: " + word_example + "\n");
                word_compo = word_compo.isEmpty() ? "" : ("Compounds: " + word_compo + "\n");
                word_idiom = word_idiom.isEmpty() ? "" : ("Idiom: " + word_idiom);

                String tmp = inflection + word_defin + word_example + word_compo + word_idiom;

                map.put(FragmentDict.KEY_WORD, word_in + " ");
                map.put(FragmentDict.KEY_WORDTYPE, word_type);
                map.put(FragmentDict.KEY_WORDTRANSLATE, word_trans);
                map.put(FragmentDict.KEY_TRANSCOMMENT, "");
                map.put(FragmentDict.KEY_PRONUCIATION, "Pronuciation: [" + phonetic + "]");
                map.put(FragmentDict.KEY_TRANSLATE, tmp);
                map.put(FragmentDict.KEY_SOUNDFILE, "");
                menuItems.add(map);
            }
        }

        return true;
    }

    private void getOfflineTransFromOneTable(SQLiteDatabase db,
                                             String tblName, String colName, String keyWord, TreeSet<Integer> idSet) {
        String strQuery = "select * from " + tblName + " where " + colName + "=\"" + keyWord + "\"";
        Cursor cursor = db.rawQuery(strQuery, null);
        while (cursor.moveToNext()) {
            int word_id = cursor.getInt(0);
            idSet.add(word_id);
        }

    }

    private String getOneRecord(SQLiteDatabase db, String query, int id, String sep) {
        String result = "";
        String strQuery = String.format(query, id);
        Cursor cur = db.rawQuery(strQuery, null);
        while (cur.moveToNext()) {
            String strSep = result.isEmpty() ? "" : sep;
            result = result + strSep + cur.getString(1);
            String col2 = cur.getString(2);
            col2 = col2.isEmpty() ? "" : (" (" + col2 + ")");
            result += col2;
        }
        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (offlineDB != null) {
            offlineDB.close();
        }
    }

    @Override
    public void onDestroyView() {
        // TODO Auto-generated method stub
        super.onDestroyView();
    }

    // when switch between Landscape and Portrait mode, the fragment will be recreated
    // so, we have to reinflate the list view according to current word
    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        // save current lookup
        pEditor.putString("lookup", edit_input.getText().toString());
        pEditor.commit();

        String strDictMode = !useOfflineDB ? "Dictionary" :"Dictionary(offline)";
        pEditor.putString("DictMode", strDictMode);
        pEditor.commit();

        // save native language
        String strNative = "NONE";
        switch (curNativeLang) {
            case AR:
                strNative = "AR";
                break;
            case CN:
                strNative = "CN";
                break;
            case NONE:
                strNative = "NONE";
                break;
            case TH:
                strNative = "TH";
                break;
        }
        pEditor.putString("NativeLang", strNative);
        pEditor.commit();

        if (m_ProgressDialog != null) {
            m_ProgressDialog.dismiss();
            m_ProgressDialog = null;
        }

    }

    private void copyOfflineDBToPhone() {
        DataBaseUtil util = new DataBaseUtil(getView().getContext(), OFFLINEDBNAME, R.raw.offlinedict);

        if (util.checkDataBase()) {
            //Log.i("tag", "---------------The database is exist.----------------");
        } else {
            try {
                util.copyDataBase();
            } catch (IOException e) {
                //System.out.println("----------------copy error!------------------------");
                throw new Error("Error copying database");
            }
        }
    }
}
