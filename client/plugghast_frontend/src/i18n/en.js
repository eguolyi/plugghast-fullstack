export default {
    App: {
        loadingLookupText: 'Looking up...',
        loadingSuggestionText: 'Fetching suggestions...',
        loadingTransPageText: 'Translating...',
        alertNetworkIssueTitle: 'Network Issue?',
        alertNetworkTimeoutText: 'Network connection time out. Please try it again later.',
        alertNetworkUnavailableText: 'Network service is unavailable. Please try it again later or contact web site admin.',
        alertNetworkIssueText: 'Network service cannot be connected. Please check your network configuration.',
        closeButtonText: 'Close',
    },
    SelectLanguageDialog: {
        dialogTitle: 'Select Native Language',
        dialogText: 'After setting native language, if you click Translation, Definition, Explanation, Examples, Idioms and Compounds in dictionary entries, Plugghäst will help translate them to your native language.',
        noNeedToSetText: 'No need',
        notSelectedText: 'Not selected',
        closeButtonText: 'Close',
        okButtonText: 'OK'
    },
    ResponsiveDrawer: {
        dictItemText: 'Dictionary',
        transItemText: 'Translate',
        newWordItemText: 'New words',
        settingsItemText: 'Settings',

        newsItemText: 'News in Swedish',
        specialtySwedish: 'Specialty Swedish',
        gameAndTestItemText: 'Quiz and game',
        tipOfTheDayItemText: 'Tip of the day',
    },
    PageContainer: {
        DictionaryPage: {
            lookupButtonText: 'Look up',
            AutoSuggestInput: {
                inputPlaceholderText: 'input Swedish or English word/phrase...'
            },
            DictionaryEntryList: {
                wordClassNoun: 'n.',
                wordClassVerb: 'v.',
                wordClassAdj: 'adj.',
                wordClassInterj: 'interj.',
                wordClassPron: 'pron.',
                wordClassAdv: 'adv.',
                wordClassConj: 'conj.',
                wordClassPrep: 'prep.',
                wordClassNum: 'num.',
                NestedList: {
                    EmbeddedList: {
                        enDefinitionText: 'Translation: ',
                        svDefinitionText: 'Definition: ',
                        synonymText: 'Synonyms: ',
                        inflectionsText: 'Inflections: ',
                        pronunciationText: 'Pronunciation: ',
                        explanationText: 'Explanation: ',
                        examplesText: 'Examples',
                        idiomsText: 'Idioms',
                        compoundsText: 'Compounds',
                        setNativeLangDialogTitle: 'Set Native Language',
                        setNativeLangDialogText: 'You can click "Set language" below to tell Plugghäst your native language. Then if you click Translation, Definition, Explanation, Examples, Idioms and Compounds in dictionary entries, Plugghäst will help translate them to your native language.',
                        noNeedToSetText: 'No need',
                        setNativeLangButtonText: 'Set language',
                    }
                }
            },
            NativeTranslateDialog: {
                svLabelText: 'sv: ',
                enLabelText: ' en: ',
                nativeTranslationTitle: 'Native Translation',
                confidenceLabelText: '. confidence: ',
                noResultsText: 'No query results',
                copyToClipboardText: 'Copy text to clipboard',
                detectBrowserLanguageDesc: 'Plugghäst can help translate Definition, Translation, Explanation, Examples, Idioms and Compounds in dictionary entries to your native language. Plugghät guesses your native language is "%s" according to your system settings. If "%s" is not your native language, you can click "Set language" below to set it. If you do not need it, please click "No need".',
                setNativeLangButtonText: 'Set language',
                noNeedToSetText: 'No need',
                closeButtonText: 'Close',
                okButtonText: 'OK'
            },
            FuzzyLookupDialog: {
                noEntryInDictText: 'No such entry in the dictionary: ',
                listSuggestionsText: '. Do you want to search one of following?',
                notInSuggestionListText: 'If none of the above is what you want, ',
                tryNativeTranslationText: 'click me to try your luck? :)',
                closeButtonText: 'Close'
            },
            SaolInflectionDialog: {
                saolDialogTitle: '\'s Different Forms',
                noResultsText: 'No query results',
                closeButtonText: 'Close'
            }
        },
        TranslatePage: {
            pageTile: 'Translate',
            pageDescription: 'Here you can translate',
            transButtonText: 'Translate',
            sameLanguagesAlert: 'The From Language and To Language must be different',
            emptyInputAlert: 'Please input the text to be translated',
            labelFrom: 'From',
            labelTo: 'To',
            placeholderFrom: 'input the text you want to translate',
            TranslateLanguageSelect: {
                englishName: 'English',
                swedishName: 'Swedish',
            },
            jump2DictButtonText: 'Look up',
            dictItemText: 'Dictionary',
        },
        NewWordsPage: {
            pageTile: 'My New Words',
            pageDescription: 'Here you can save your new words and review them'
        },
        SettingsPage: {
            pageTile: 'Settings',
            pageDescription: 'App settings',
            setNativeLangHead: 'Native language',
            setNativeLangDesc: 'After setting your native language, if you click Translation, Definition, Explanation, Examples, Idioms and Compounds in dictionary entries, Plugghäst will help translate them to your native language.',
            setUILanguageHead: 'UI language',
            setUILanguageDesc: 'The language Plugghäst is using in UI elements, such as dialogs or buttons. Because of our limited language level, in this version we provide only two UI lanugage options : English and Chinese。If you are interested，welcome to join us to make Plugghäst be easy to use for more people:)',
            aboutHead: 'About Plugghäst',
            aboutDesc: '',
            userManualHead: 'How to use',
            userManualDesc: 'How to use',
            contactHead: 'Contact author',
            contactDesc: 'If you have any comments or suggestions on Plugghäst, please feel free to send email to plugghast.nu@gmail.com',
            donationHead: 'Donation',
            donationDesc: 'All of your donation will be used to cover the cost of the cloud service, domain name and other maintenance fee',
            notSelectedText: 'Not selected'
        },
        NewsPage: {
            pageTile: 'News',
            pageDescription: 'Read news in Swedish'
        },
        TipsPage: {
            pageTile: 'Tips of the Day',
            pageDescription: 'useful tips'
        },
        QuestionPage: {
            pageTile: 'Ask Question',
            pageDescription: 'Ask questions'
        }
    }
}