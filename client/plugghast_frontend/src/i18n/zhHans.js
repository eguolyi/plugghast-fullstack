export default {
    App: {
        loadingLookupText: '查询中...',
        loadingSuggestionText: '获取输入建议...',
        loadingTransPageText: '正在翻译...',
        alertNetworkIssueTitle: '网络连接问题？',
        alertNetworkTimeoutText: '网络连接超时，请稍后再试。',
        alertNetworkUnavailableText: '网络服务暂时不可用，请稍后再试或联系网站管理员。',
        alertNetworkIssueText: '网络无法连接。请检查网络设置是否正常。',
        closeButtonText: '关闭',
    },
    SelectLanguageDialog: {
        dialogTitle: '设置母语',
        dialogText: '在指定母语之后，如果您点击字典词条中的英语/瑞典语释义，例句，成语，复合词，Plugghäst将为您把相应信息翻译成您的母语。',
        noNeedToSetText: '不设置',
        notSelectedText: '未设置',
        closeButtonText: '关闭',
        okButtonText: '确定'
    },
    ResponsiveDrawer: {
        dictItemText: '瑞典语字典',
        transItemText: '瑞典语翻译',
        newWordItemText: '云端生词本',
        settingsItemText: '程序设置',
        newsItemText: '瑞典语新闻',
        specialtySwedish: '专业瑞典语',
        gameAndTestItemText: '测验与游戏',
        tipOfTheDayItemText: '每日推荐',
    },
    PageContainer: {
        DictionaryPage: {
            lookupButtonText: '查字典',
            AutoSuggestInput: {
                inputPlaceholderText: '输入待查瑞典语或英文单词/词组...'
            },
            DictionaryEntryList: {
                wordClassNoun: '名词',
                wordClassVerb: '动词',
                wordClassAdj: '形容词',
                wordClassInterj: '叹词',
                wordClassPron: '代词',
                wordClassAdv: '副词',
                wordClassConj: '连词',
                wordClassPrep: '介词',
                wordClassNum: '数词',
                NestedList: {
                    EmbeddedList: {
                        enDefinitionText: '英语释义: ',
                        svDefinitionText: '瑞典语释义: ',
                        synonymText: '同义词: ',
                        inflectionsText: '词形变化: ',
                        pronunciationText: '发音: ',
                        explanationText: '瑞典语解释: ',
                        examplesText: '例句',
                        idiomsText: '成语',
                        compoundsText: '复合词',
                        setNativeLangDialogTitle: '设置母语',
                        setNativeLangDialogText: '您可以点击下面的"设置母语"来告诉Plugghäst您的母语。设定后，当您点击字典词条中的英语/瑞典语释义，例句，成语，复合词，Plugghäst将为您把相应信息翻译成您的母语。',
                        noNeedToSetText: '不设置',
                        setNativeLangButtonText: '设置母语',

                    }
                }
            },
            NativeTranslateDialog: {
                svLabelText: '瑞: ',
                enLabelText: ' 英: ',
                nativeTranslationTitle: '母语翻译',
                confidenceLabelText: '. 可信度: ',
                noResultsText: '没有查询结果',
                copyToClipboardText: '拷贝文字',
                detectBrowserLanguageDesc: 'Plugghäst可以为您把字典中的英语/瑞典语释义，例句，成语和复合词翻译成您的母语。Plugghäst根据您的系统设置猜测你的母语为"%s"。如果"%s"不是您的母语，您可以点击下面的"设置母语"进行设置。如果您不需要该功能，请点击"不设置"。',
                setNativeLangButtonText: '设置母语',
                noNeedToSetText: '不设置',
                closeButtonText: '关闭',
                okButtonText: 'OK'
            },
            FuzzyLookupDialog: {
                noEntryInDictText: '字典中无词条',
                listSuggestionsText: '。您想查的是不是下列之一？',
                notInSuggestionListText: '如果上面这些都不是你想查的，',
                tryNativeTranslationText: '点我试试运气吧？:)',
                closeButtonText: '关闭'
            },
            SaolInflectionDialog: {
                saolDialogTitle: '的各种变形',
                noResultsText: '没有查询结果',
                closeButtonText: '关闭'
            }
        },
        TranslatePage: {
            pageTile: '翻译',
            pageDescription: 'Here you can translate',
            transButtonText: '翻译',
            sameLanguagesAlert: '翻译的源语言和目的语言应该不相同',
            emptyInputAlert: '请输入待翻译文字',
            labelFrom: '源语言',
            labelTo: '目的语言',
            placeholderFrom: '请输入您要翻译的文字',
            TranslateLanguageSelect: {
                englishName: '英语',
                swedishName: '瑞典语'
            },
            jump2DictButtonText: '查字典',
            dictItemText: '瑞典语字典',
        },
        NewWordsPage: {
            pageTile: '生词本',
            pageDescription: 'Here you can save your new words and review them'
        },
        SettingsPage: {
            pageTile: '设置',
            pageDescription: 'App Settings',
            setNativeLangHead: '母语',
            setNativeLangDesc: '在指定母语之后，如果您点击字典词条中的英语/瑞典语释义，例句，成语，复合词，Plugghäst将为您把相应信息翻译成您的母语。',
            setUILanguageHead: '界面语言',
            setUILanguageDesc: 'Plugghäst界面文字（如对话框和按钮等）所使用的语言。限于我们的能力水平，我们目前只提供了英文和中文两种选择。如果您感兴趣，欢迎加入我们，方便更多朋友使用Plugghäst :)',
            aboutHead: '关于Plugghäst',
            aboutDesc: '',
            userManualHead: '如何使用',
            userManualDesc: '如何使用',
            contactHead: '联系作者',
            contactDesc: '如果您有任何关于Plugghäst的意见和建议，欢迎您致信plugghast.nu@gmail.com',
            donationHead: '捐赠',
            donationDesc: '您的捐赠将全部用来支付网站服务器、域名和维护费用',
            notSelectedText: '未设置'
        },
        NewsPage: {
            pageTile: '新闻',
            pageDescription: 'Read news in Swedish'
        },
        TipsPage: {
            pageTile: '每日一贴',
            pageDescription: 'useful tips'
        },
        QuestionPage: {
            pageTile: '论坛问道',
            pageDescription: 'Ask questions'
        }
    }
}