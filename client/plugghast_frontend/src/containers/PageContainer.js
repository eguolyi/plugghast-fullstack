import React from "react"
import PropTypes from "prop-types"
import InformationPaper from '../components/pages/InformationPaper'
import DictionaryPage from '../components/pages/DictionaryPage'
import TranslatePage from '../components/pages/TranslatePage'
import SettingsPage from '../components/pages/SettingsPage'
import {pageName} from "./App"
import {connect} from "react-redux"
import { MSAPI_REQUEST_TRANS_PAGE_TYPE } from '../actions/actions'
import { isObjectEmpty } from '../common/util'
import {Close} from "@material-ui/icons"
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'


class PageContainer extends React.Component{
    static propTypes = {
        i18nStrings: PropTypes.object.isRequired,
        currentPage: PropTypes.symbol.isRequired,
        //nativeLanguage: PropTypes.string.isRequired
        msApiDetails: PropTypes.object.isRequired,
        msApiIsFetching: PropTypes.bool.isRequired,
        msApiNativeText: PropTypes.string.isRequired,

        transPageFromText: PropTypes.string.isRequired,
        transPageDetails: PropTypes.object.isRequired,
        transPageIsFetching: PropTypes.bool.isRequired,
        transPageFromLang: PropTypes.string.isRequired,
        transPageToLang: PropTypes.string.isRequired,

        handleNativeLangChange: PropTypes.func.isRequired,
        nativeLanguage: PropTypes.string.isRequired,

        onOpenSelectNativeDialog: PropTypes.func.isRequired,
        openAlertDialog: PropTypes.func.isRequired,

        onSelectUiLang: PropTypes.func.isRequired,
        uiLanguage: PropTypes.string.isRequired,

        handlePageChange: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            openSnackBar: false,
            snackbarMessage: ''
        }
    }

    onCloseSnackBar = () => {
        this.setState((state) => ({
            ...state,
            snackbarMessage: '',
            openSnackBar: false
        }))
    }

    onOpenSnackBar = (newMessage) => {
        this.setState((state) => ({
            ...state,
            snackbarMessage: newMessage,
            openSnackBar: true
        }))
    }

    renderPage = () => {
        const { i18nStrings, nativeLanguage, currentPage, msApiIsFetching, msApiNativeText, msApiDetails,
            transPageFromText, transPageIsFetching, transPageDetails, dispatch, onOpenSelectNativeDialog,
            handleNativeLangChange, openAlertDialog, onSelectUiLang, uiLanguage, transPageFromLang, transPageToLang, handlePageChange } = this.props
        switch (currentPage) {
            case pageName.DICTIONARY:
                return (
                    <DictionaryPage
                        msApiDetails={msApiDetails}
                        msApiIsFetching={msApiIsFetching}
                        msApiNativeText={msApiNativeText}
                        i18nStrings={i18nStrings['DictionaryPage']}
                        currentPage={currentPage}
                        onOpenSelectNativeDialog={onOpenSelectNativeDialog}
                        handleNativeLangChange={handleNativeLangChange}
                        openAlertDialog={openAlertDialog}
                    />
                    )
            case pageName.TRANSLATE:
                return (
                    <TranslatePage
                        msApiIsFetching={transPageIsFetching}
                        msApiNativeText={transPageFromText}
                        msApiDetails={transPageDetails}
                        dispatch={dispatch}
                        i18nStrings={i18nStrings['TranslatePage']}
                        nativeLanguage={nativeLanguage}
                        onOpenSnackbar={this.onOpenSnackBar}
                        openAlertDialog={openAlertDialog}
                        transPageFromLang={transPageFromLang}
                        transPageToLang={transPageToLang}
                        handlePageChange={handlePageChange}
                    />
                )
            case pageName.SETTINGS:
                return (
                    <SettingsPage
                        nativeLanguage={nativeLanguage}
                        i18nStrings={i18nStrings['SettingsPage']}
                        handleNativeLangChange={handleNativeLangChange}
                        onOpenSnackbar={this.onOpenSnackBar}
                        onSelectUiLang={onSelectUiLang}
                        uiLanguage={uiLanguage}
                    />
                )
            case pageName.TIPS:
            case pageName.NEWS:
            case pageName.QUIZ_GAME:
            case pageName.SPECIALTY:
            case pageName.NEW_WORDS:
            default:
                return (
                    <InformationPaper
                        infoTitle={'title ' + currentPage.description}
                        infoText={'text text'}/>
                )
        }
    }

    render() {
        return (
            <div>
                {
                    this.renderPage()
                }
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    open={this.state.openSnackBar}
                    autoHideDuration={10000}
                    onClose={this.onCloseSnackBar}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<div id="message-id" style={{textAlign: 'left'}}>{this.state.snackbarMessage}</div>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.onCloseSnackBar}
                        >
                            <Close />
                        </IconButton>,
                    ]}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { dataBySource } = state

    let msApiIsFetching, msApiNativeText, msApiDetails, transPageFromText, transPageDetails, transPageIsFetching, transPageFromLang, transPageToLang
    msApiIsFetching = true
    msApiNativeText = ''
    msApiDetails = {}

    transPageIsFetching = false
    transPageFromText = ''
    transPageDetails = {}
    transPageFromLang = ''
    transPageToLang = ''

    if (dataBySource['msApi'] && !isObjectEmpty(dataBySource['msApi'])) {
        let msApiRequstType = dataBySource['msApi']['selectRequestType']
        let { isFetching, nativeText, details, translateToLang, translateFromLang } = getMsApiResultsFromData(dataBySource, msApiRequstType)
        if (msApiRequstType === MSAPI_REQUEST_TRANS_PAGE_TYPE) {
            transPageIsFetching = isFetching
            transPageFromText = nativeText
            transPageDetails = details
            transPageFromLang = translateFromLang
            transPageToLang = translateToLang
        } else {
            msApiIsFetching = isFetching
            msApiNativeText = nativeText
            msApiDetails = details
        }
    }

    return {
        msApiDetails,
        msApiNativeText,
        msApiIsFetching,

        transPageIsFetching,
        transPageFromText,
        transPageDetails,
        transPageFromLang,
        transPageToLang
    }
}

function getMsApiResultsFromData(dataBySource, msApiRequstType) {
    let isFetching, presentIndex, nativeText, details, translateFromLang, translateToLang
    isFetching = false
    nativeText = ''
    details = {}
    translateFromLang = ''
    translateToLang = ''

    if (dataBySource['msApi'][msApiRequstType] && !isObjectEmpty(dataBySource['msApi'][msApiRequstType])) {
        isFetching = dataBySource['msApi'][msApiRequstType].isFetching
        presentIndex = dataBySource['msApi'][msApiRequstType].presentIndex
        nativeText = dataBySource['msApi'][msApiRequstType].history[presentIndex].keyWord
        if (isFetching) {
            details = {}
        } else {
            const translateData = dataBySource['msApi'][msApiRequstType].history[presentIndex].details
            const isDetailsEmpty = isObjectEmpty(translateData)
            details = (translateData && !isDetailsEmpty) ? translateData[0] : {}  // it is an array which has only one element

            if (msApiRequstType === MSAPI_REQUEST_TRANS_PAGE_TYPE && !isDetailsEmpty) {
                translateFromLang = dataBySource['msApi'][msApiRequstType].history[presentIndex].srcLang
                translateToLang = dataBySource['msApi'][msApiRequstType].history[presentIndex].toLang
            }
        }
    }

    return {
        isFetching,
        nativeText,
        details,
        translateToLang,
        translateFromLang
    }
}

export default connect(mapStateToProps)(PageContainer)