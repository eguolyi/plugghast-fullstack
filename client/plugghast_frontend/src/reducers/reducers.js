import { combineReducers } from "redux"
import { LOOKUP_KEY, RECEIVE_DETAILS, MARK_REQUEST_STATUS, SHOW_REQUEST_FAILURE,
    REQUEST_DETAILS, REQUEST_FAILURE, SELECT_DATA_SOURCE, DATA_SOURCE_FOLKLEX,
    SET_NATIVE_LANGUAGE, SET_UI_LANGUAGE, SET_TEXT_TRANSLATE_ENGINE, CLEAN_HISTORY_DATA, SHOW_FUZZY_DIALOG,
    MSAPI_REQUEST_TRANS_PAGE_TYPE, GAPI_REQUEST_TRANS_TYPE } from "../actions/actions"

//const historyLength = 20

function nativeLanguage(state = '', action) {
    switch (action.type) {
        case SET_NATIVE_LANGUAGE:
            return action.lang
        default:
            return state
    }
}

function uiLanguage(state = '', action) {
    switch (action.type) {
        case SET_UI_LANGUAGE:
            return action.lang
        default:
            return state
    }
}

function textTranslateEngine(state = 'ms', action) {
    switch (action.type) {
        case SET_TEXT_TRANSLATE_ENGINE:
            return action.engine
        default:
            return state
    }
}

// the status of the current request
const initalRequestState = {
    code: 200,
    source: '',
    isShown: false
}
function currentRequestStatus(state = initalRequestState, action) {
    switch (action.type) {
        case MARK_REQUEST_STATUS:
            return {
                source: action.dataSource,
                code: action.code,
                isShown: false
            }
        case SHOW_REQUEST_FAILURE:
            return {
                ...state,
                isShown: action.isShown
            }
        default:
            return state
    }
}

function dataSource(state = DATA_SOURCE_FOLKLEX, action) {
    switch (action.type) {
        case SELECT_DATA_SOURCE:
            return action.dataSource
        default:
            return state
    }
}

function dataBySource(state = {}, action) {
    switch (action.type) {
        case LOOKUP_KEY:
        case REQUEST_DETAILS:
        case RECEIVE_DETAILS:
        case REQUEST_FAILURE:
        case SHOW_FUZZY_DIALOG:
           return{
                ...state,
                [action.dataSource]: getData(state[action.dataSource], action)
            }
        case CLEAN_HISTORY_DATA:
            return{
                ...state,
                [action.dataSource]: {}
            }
        default:
            return state
    }
}

function getData(state = {}, action ) {
    switch (action.type) {
        case LOOKUP_KEY:
        case REQUEST_DETAILS:
        case RECEIVE_DETAILS:
        case REQUEST_FAILURE:
        case SHOW_FUZZY_DIALOG:
            return {
                ...state,
                selectRequestType: action.requestType,
                [action.requestType]: getDetails(state[action.requestType], action)
            }
        default:
            return state
    }
}

const initialDetailsState = {
    isFetching: false,
    presentIndex: -1,
    history: []
}

function getDetails(state = initialDetailsState, action ) {
    switch (action.type) {
        case LOOKUP_KEY:
            let index = searchLookupHistory(state.history, action.keyWord, action.requestType, action.srcLang, action.toLang)
            if (index === -1) { // not in history so create a new node
                return {
                    ...state,
                    presentIndex: state.history.length,
                    history: state.history.concat([{
                        keyWord: action.keyWord,
                        details: {}
                    }])
                }
            } else {    // already in history
                let historyNode = state.history[index]
                if (historyNode.details instanceof Array) { // it is a 'suggestions' node
                    historyNode.isShown = false // the keyWord is requested again. Let's show the fuzzy dialog again
                }
                return {
                    ...state,
                    presentIndex: index
                }
            }
        case REQUEST_DETAILS:
            return {
                ...state,
                isFetching: true,
            }
        case RECEIVE_DETAILS:
            state.history[state.presentIndex].details = action.details
            if (action.details instanceof Array) {  // the key word has no results. We need to show the suggestion is an alert dialog
                state.history[state.presentIndex].isShown = false
            }

            // for translate page request, we need to mark languages (otherwise uers cannot translate again)
            if (action.requestType === MSAPI_REQUEST_TRANS_PAGE_TYPE || action.requestType === GAPI_REQUEST_TRANS_TYPE ) {
                state.history[state.presentIndex].srcLang = action.srcLang
                state.history[state.presentIndex].toLang = action.toLang
            }

            return {
                ...state,
                isFetching: false,
            }
        case REQUEST_FAILURE:
            return {
                ...state,
                isFetching: false,
            }
        case SHOW_FUZZY_DIALOG:
            let indexFuzzy = searchLookupHistory(state.history, action.keyWord)
            if (indexFuzzy !== -1) {
                if (state.history[indexFuzzy].details instanceof Array) { //find the history record of the key word
                    state.history[indexFuzzy].isShown = true
                }
            }

            return state
        default:
            return state
    }
}

// search lookup history with current key word. If the key word is in the history, return the index. If not, return -1
function searchLookupHistory(history, keyWord, requestType, srcLang, toLang) {
    if (history.length === 0) {
        return -1
    }

    return history.findIndex(node => {
        if (requestType !== MSAPI_REQUEST_TRANS_PAGE_TYPE && requestType !== GAPI_REQUEST_TRANS_TYPE) {
            return node.keyWord === keyWord
        } else { // in Translate page we need to check keyWord, srcLang and toLang
            return node.keyWord === keyWord
                && node.srcLang === srcLang
                && node.toLang === toLang
        }

    })
}

const rootReducer = combineReducers({
    dataSource,
    dataBySource,
    currentRequestStatus,
    nativeLanguage,
    uiLanguage,
    textTranslateEngine
})

export default rootReducer
