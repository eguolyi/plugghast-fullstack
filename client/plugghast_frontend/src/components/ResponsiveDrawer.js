import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {Book, Translate, EventNote, Settings, FiberNew, Mouse, QuestionAnswer, AssignmentInd} from "@material-ui/icons";
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import logo from '../images/logo.png'
import { pageName } from "../containers/App"


export const drawerWidth = 240;
export const appbarHeigh = 64;

const DICT_ITEM_TEXT_ID = 'dictItemText'
const TRANS_ITEM_TEXT_ID = 'transItemText'
const NEWWORD_ITEM_TEXT_ID = 'newWordItemText'
const SETTINGS_ITEM_TEXT_ID = 'settingsItemText'
const NEWS_ITEM_TEXT_ID = 'newsItemText'
const TIP_ITEM_TEXT_ID = 'tipOfTheDayItemText'
const SPECIALTY_SWEDISH_TEXT_ID = 'specialtySwedish'
const GAME_QUIZ_TEXT_ID = 'gameAndTestItemText'
//const ASK_ITEM_TEXT_ID = 'askQuestionItemText'


const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            height: appbarHeigh
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
});

const iconComponentsMain = [Book, Translate, EventNote, Settings]
const iconComponentsOther = [FiberNew, AssignmentInd, Mouse, QuestionAnswer]

class ResponsiveDrawer extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        theme: PropTypes.object.isRequired,

        nativeLanguage: PropTypes.string.isRequired,

        i18nStrings: PropTypes.object.isRequired,

        handlePageChange: PropTypes.func.isRequired
    }

    constructor (props) {
        super(props)

        this.state = {
            mobileOpen: false
        }
    }

    handleDrawerToggle = () => {
        this.setState(state => ({
            ...state,
            mobileOpen: !state.mobileOpen
        }));
    }

    handleDrawerItemClick = (i18nStrings, text) => {
        this.props.handlePageChange(this.getPageNameFromItemText(i18nStrings, text), text)

        this.setState(state => ({
            ...state,
            mobileOpen: false
        }));
    }

    getPageNameFromItemText = (i18nStrings, text) => {
        switch (text) {
            case i18nStrings[DICT_ITEM_TEXT_ID]:
                return pageName.DICTIONARY
            case i18nStrings[TRANS_ITEM_TEXT_ID]:
                return pageName.TRANSLATE
            case i18nStrings[NEWWORD_ITEM_TEXT_ID]:
                return pageName.NEW_WORDS
            case i18nStrings[SETTINGS_ITEM_TEXT_ID]:
                return pageName.SETTINGS
            case i18nStrings[NEWS_ITEM_TEXT_ID]:
                return pageName.NEWS
            case i18nStrings[TIP_ITEM_TEXT_ID]:
                return pageName.TIPS
            case i18nStrings[SPECIALTY_SWEDISH_TEXT_ID]:
                return pageName.SPECIALTY
            case i18nStrings[GAME_QUIZ_TEXT_ID]:
                return pageName.QUIZ_GAME
            default:
                return null
        }
    }

    generateMenuItem = (i18nStrings, itemStrings, iconComponents) => {
        return (
            <List>
                {
                    itemStrings.map((text, i) => (
                        <ListItem button key={i} >
                            <ListItemIcon>{
                                (() => {
                                    let DrawerIcon = iconComponents[i]
                                    return <DrawerIcon />
                                })()

                            }</ListItemIcon>
                            <ListItemText primary={text} onClick={() => this.handleDrawerItemClick(i18nStrings, text)}/>
                        </ListItem>
                    ))}
            </List>
        );
    }

    render() {
        const { classes, theme, i18nStrings } = this.props


        const drawer = (
            <div>
                <div className={classes.toolbar} style={{ display: 'flex', alignItems: 'center'}} >
                    <img style={{marginLeft: 20}} src={logo} alt="Logo" />
                    <Typography variant="h5" color="primary" noWrap >
                        Plugghäst
                    </Typography>

                </div>
                <Divider />
                {this.generateMenuItem(i18nStrings,[i18nStrings[DICT_ITEM_TEXT_ID],
                                                    i18nStrings[TRANS_ITEM_TEXT_ID],
                                                    i18nStrings[NEWWORD_ITEM_TEXT_ID],
                                                    i18nStrings[SETTINGS_ITEM_TEXT_ID]], iconComponentsMain)}
                <Divider />
                {this.generateMenuItem(i18nStrings,[i18nStrings[NEWS_ITEM_TEXT_ID],
                                                    i18nStrings[SPECIALTY_SWEDISH_TEXT_ID],
                                                    i18nStrings[GAME_QUIZ_TEXT_ID],
                                                    i18nStrings[TIP_ITEM_TEXT_ID]], iconComponentsOther)}
            </div>
        );

        return (
            <div className={classes.root}>
                <CssBaseline />
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" noWrap id="header">
                            {this.props.i18nStrings[DICT_ITEM_TEXT_ID]}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <nav className={classes.drawer}>
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Hidden smUp implementation="css">
                        <Drawer
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(ResponsiveDrawer);