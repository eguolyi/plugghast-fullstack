import React from "react"
import PropTypes from "prop-types"
import {appbarHeigh, drawerWidth} from "../ResponsiveDrawer"
import Paper from '@material-ui/core/Paper'
import {withStyles} from "@material-ui/core"
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar'
import Divider from '@material-ui/core/Divider'
import { Translate, InfoOutlined, ContactSupport, ContactMail, AttachMoney, Language, LibraryBooks } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton'
import NativeLanguageSelect from '../small/NativeLanguageSelect'
import CustomSelect from '../small/CustomSelect'
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import donation from '../../images/donation.png'

const styles = theme => ({
    nested: {
        flex: 1,
        [theme.breakpoints.down('sm')]: {
            marginRight: 10,
            marginLeft: 10
        },
        [theme.breakpoints.up('sm')]: {
            marginRight: 100,
            marginLeft: drawerWidth + 100
        }
    }
});

const SET_NATIVE_LANG_HEAD_TEXT_ID = 'setNativeLangHead'
const SET_NATIVE_LANG_DESC_TEXT_ID = 'setNativeLangDesc'
const SET_UI_LANG_HEAD_TEXT_ID = 'setUILanguageHead'
const SET_UI_LANG_DESC_TEXT_ID = 'setUILanguageDesc'
const USER_MANUAL_HEAD_TEXT_ID = 'userManualHead'
const USER_MANUAL_DESC_TEXT_ID = 'userManualDesc'


const ABOUT_HEAD_TEXT_ID = 'aboutHead'
const CONTACT_AUTHOR_HEAD_TEXT_ID = 'contactHead'
const CONTACT_AUTHOR_DESC_TEXT_ID = 'contactDesc'
const DONATION_HEAD_TEXT_ID = 'donationHead'

class SettingsPage extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        nativeLanguage: PropTypes.string.isRequired,
        i18nStrings: PropTypes.object.isRequired,
        handleNativeLangChange: PropTypes.func.isRequired,
        onOpenSnackbar: PropTypes.func.isRequired,
        onSelectUiLang: PropTypes.func.isRequired,
        uiLanguage: PropTypes.string.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            expandAbout: false,
            expandManual: false,
            expandContact: false,
            expandDonation: true
        }
    }

    onExpandSection = name => () => {
        this.setState(
            state => ({
                ...state,
                [name]: !state[name]
            })
        )
    }

    onSelectTextTranslateEngine = (newEngine) => {

    }

    render() {
        const { classes, i18nStrings, nativeLanguage, uiLanguage, handleNativeLangChange, onOpenSnackbar, onSelectUiLang } = this.props
        return (
            <div className={classes.nested}>
                <Paper style={{ marginTop: appbarHeigh + 10}}>
                    <List component="nav"  aria-label="Mailbox folders">
                        <ListItem >
                            <ListItemAvatar>
                                <Avatar>
                                    <Translate />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[SET_NATIVE_LANG_HEAD_TEXT_ID]} style={{marginRight: 10}}/>
                            <NativeLanguageSelect
                                nativeLanguage={nativeLanguage}
                                i18nStrings={i18nStrings}
                                selectNativeLang={handleNativeLangChange}/>
                            <IconButton size="small" color="primary" onClick={() => onOpenSnackbar(i18nStrings[SET_NATIVE_LANG_DESC_TEXT_ID])}>
                                <InfoOutlined />
                            </IconButton>
                        </ListItem>
                        <Divider />
                        <ListItem >
                            <ListItemAvatar>
                                <Avatar>
                                    <Language />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[SET_UI_LANG_HEAD_TEXT_ID]} style={{marginRight: 10}}/>
                            <CustomSelect
                                initialValue={uiLanguage}
                                optionValues={['zh-Hans', 'en']}
                                optionTexts={['中文', 'English']}
                                onSelectOption={onSelectUiLang}/>
                            <IconButton size="small" color="primary" onClick={() => onOpenSnackbar(i18nStrings[SET_UI_LANG_DESC_TEXT_ID])}>
                                <InfoOutlined />
                            </IconButton>
                        </ListItem>
                        <Divider />
                        <ListItem >
                            <ListItemAvatar>
                                <Avatar>
                                    <Language />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={'文字翻译引擎'} style={{marginRight: 10}}/>
                            <CustomSelect
                                initialValue={'ms'}
                                optionValues={['ms', 'google']}
                                optionTexts={['微软Bing', '谷歌翻译']}
                                onSelectOption={this.onSelectTextTranslateEngine}/>
                            <IconButton size="small" color="primary" onClick={() => onOpenSnackbar(i18nStrings[SET_UI_LANG_DESC_TEXT_ID])}>
                                <InfoOutlined />
                            </IconButton>
                        </ListItem>
                        <Divider />
                        <ListItem button onClick={this.onExpandSection('expandAbout')}>
                            <ListItemAvatar>
                                <Avatar>
                                    <ContactSupport />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[ABOUT_HEAD_TEXT_ID]} />
                            {this.state.expandAbout ? <ExpandMore /> : <ExpandLess />}
                        </ListItem>
                        <Collapse in={this.state.expandAbout} timeout="auto" unmountOnExit >
                            <p>About Plugghäst</p>
                        </Collapse>
                        <Divider />
                        <ListItem button onClick={this.onExpandSection('expandManual')}>
                            <ListItemAvatar>
                                <Avatar>
                                    <LibraryBooks />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[USER_MANUAL_HEAD_TEXT_ID]} />
                            {this.state.expandManual ? <ExpandMore /> : <ExpandLess />}
                        </ListItem>
                        <Collapse in={this.state.expandManual} timeout="auto" unmountOnExit >
                            <p>{i18nStrings[USER_MANUAL_DESC_TEXT_ID]}</p>
                        </Collapse>
                        <Divider />
                        <ListItem button onClick={this.onExpandSection('expandContact')}>
                            <ListItemAvatar>
                                <Avatar>
                                    <ContactMail />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[CONTACT_AUTHOR_HEAD_TEXT_ID]} />
                            {this.state.expandContact ? <ExpandMore /> : <ExpandLess />}
                        </ListItem>
                        <Collapse in={this.state.expandContact} timeout="auto" unmountOnExit >
                            <p>{i18nStrings[CONTACT_AUTHOR_DESC_TEXT_ID]}</p>
                        </Collapse>
                        <Divider />
                        <ListItem button onClick={this.onExpandSection('expandDonation')}>
                            <ListItemAvatar>
                                <Avatar>
                                    <AttachMoney />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={i18nStrings[DONATION_HEAD_TEXT_ID]} />
                            {this.state.expandDonation ? <ExpandMore /> : <ExpandLess />}
                        </ListItem>
                        <Collapse in={this.state.expandDonation} timeout="auto" unmountOnExit >
                            <div style={{ marginTop: 50, display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                <img src={donation} alt="Donation QR code" />
                            </div>
                        </Collapse>
                    </List>
                </Paper>

            </div>
        )
    }
}

export default withStyles(styles)(SettingsPage)