import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ReactCountryFlag from "react-country-flag";
import EmbeddedList from './EmbeddedList'

function getCountryCodeFromEntryLang(entryLang) {
    let countryCode = 'se'
    if ('en' === entryLang) {
        countryCode = 'gb'
    }
    return countryCode
}

function isEntryListExpandable(entryKey) {
    switch (entryKey) {
        case 'examples':
        case 'idioms':
        case 'compounds':
            return true
        default:
            return false
    }
}


class NestedList extends React.Component {
    static propTypes = {
        headString: PropTypes.string.isRequired,
        entryLang: PropTypes.string.isRequired,
        entryText: PropTypes.string.isRequired,     // the dictionary entry text. For a verb as an example,, it should be the verb's infinit form
        entryClass: PropTypes.string.isRequired,    // verb, noun, adj., ...
        wordDetails: PropTypes.object.isRequired,
        openNativeDialog: PropTypes.func.isRequired,
        dispatch: PropTypes.func.isRequired,
        nativeLanguage: PropTypes.string.isRequired,
        openAlertDialog: PropTypes.func,
        openSaolInflectionDialog: PropTypes.func,
        setCurrentEntryText: PropTypes.func,
        i18nStrings: PropTypes.object.isRequired,
        onOpenSelectNativeDialog: PropTypes.func.isRequired
    }

    state = {
        open: false,
    };

    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
    };

    render() {
        return (
            <List component="div" >
                <ListItem button onClick={this.handleClick}>
                    <ListItemIcon>
                        <ReactCountryFlag code={getCountryCodeFromEntryLang(this.props.entryLang)} svg />
                    </ListItemIcon>
                    <ListItemText primary={this.props.headString} secondary={this.state.open ? "" : this.props.wordDetails['translation']}/>
                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit >
                    {Object.keys(this.props.wordDetails).map((key, i) =>
                        <EmbeddedList
                            key={i}
                            headString={this.props.headString}
                            entryKey={key}
                            entryDetails={this.props.wordDetails[key]}
                            entryLang={this.props.entryLang}
                            entryText={this.props.entryText}
                            entryClass={this.props.entryClass}
                            expandable={isEntryListExpandable(key)}
                            openNativeDialog={this.props.openNativeDialog}
                            dispatch={this.props.dispatch}
                            nativeLanguage={this.props.nativeLanguage}
                            openAlertDialog={this.props.openAlertDialog}
                            openSaolInflectionDialog={this.props.openSaolInflectionDialog}
                            setCurrentEntryText={this.props.setCurrentEntryText}
                            i18nStrings={this.props.i18nStrings['EmbeddedList']}
                            onOpenSelectNativeDialog={this.props.onOpenSelectNativeDialog}
                        />
                    )}
                </Collapse>
            </List>
        );
    }
}

export default NestedList