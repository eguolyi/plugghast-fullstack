/**
 * this list is for the sub entries of a word. It is embeded in the word entry.
 **/
import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import {Book, ImportContacts, VolumeUp, Translate, More, ExpandMore, ExpandLess} from '@material-ui/icons'
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core";
import {polishText, isStringText, saveStringToLocalStorage, LOCAL_STORAGE_KEY_NAME} from '../../common/util'
import {
    DATA_SOURCE_SAOL,
    DATA_SOURCE_MSAPI,
    fetchDetailsIfNeeded,
    lookupKey,
    MSAPI_REQUEST_DICT_TYPE,
    MSAPI_REQUEST_TRANS_TYPE,
    SAOL_REQUEST_INFLECTION_TYPE,
    selectDataSource, setNativeLanguage, cleanHistoryData
} from "../../actions/actions"
import {ALERT_TYPE} from '../../containers/App'


const urlSoundfilePrefix = 'http://lexin.nada.kth.se/sound/'

const EN_DEFINITION_TEXT_ID = 'enDefinitionText'
const SV_DEFINITION_TEXT_ID = 'svDefinitionText'
const SYNONYM_TEXT_ID = 'synonymText'
const INFLECTIONS_TEXT_ID = 'inflectionsText'
const PRONUNCIATION_TEXT_ID = 'pronunciationText'
const EXPLANATION_TEXT_ID = 'explanationText'
const EXAMPLES_TEXT_ID = 'examplesText'
const IDIOMS_TEXT_ID = 'idiomsText'
const COMPOUNDS_TEXT_ID = 'compoundsText'
const SET_NATIVE_LANG_DIALOG_TITLE_ID = 'setNativeLangDialogTitle'
const SET_NATIVE_LANG_DIALOG_TEXT_ID = 'setNativeLangDialogText'
const NO_NEED_TEXT_ID = 'noNeedToSetText'
const SET_NATIVE_LANG_BUTTON_TEXT_ID = 'setNativeLangButtonText'


const styles = theme => ({
    embedded: {
        paddingLeft: theme.spacing(2),
    },
    subentry: {
        paddingLeft: theme.spacing(4),
    }
});

function getSoundFileUrl(soundData) {
    if (soundData['soundFile']) {
        let fileName = soundData['soundFile'].replace('swf', 'mp3')
        fileName = fileName.replace(/å/g, '0345')
        fileName = fileName.replace(/ä/g, '0344')
        fileName = fileName.replace(/ö/g, '0366')
        return urlSoundfilePrefix + fileName
    }

    return ''
}

class EmbeddedList extends React.Component{
    static propTypes = {
        classes: PropTypes.object.isRequired,
        headString: PropTypes.string.isRequired,    // the string shown in the parent item (normally it is the Swedish entry word/text
        entryKey: PropTypes.string.isRequired,      // the name of the entry
        entryLang: PropTypes.string.isRequired,     // en or sv?
        entryText: PropTypes.string.isRequired,     // the dictionary entry text. For a verb as an example,, it should be the verb's infinit form
        entryClass: PropTypes.string.isRequired,    // verb, noun, adj., ...
        //entryDetails: PropTypes.object,             // the data for the entry which will be used to populate the list
        openNativeDialog: PropTypes.func.isRequired,
        expandable: PropTypes.bool.isRequired,      // if this list is expandable. For example, examples entry
        dispatch: PropTypes.func.isRequired,
        nativeLanguage: PropTypes.string.isRequired,
        openAlertDialog: PropTypes.func,
        openSaolInflectionDialog: PropTypes.func,
        setCurrentEntryText: PropTypes.func,
        i18nStrings: PropTypes.object.isRequired,
        onOpenSelectNativeDialog: PropTypes.func.isRequired
    }

    constructor (props) {
        super(props)
        this.state = {
            open: false,        // if expandable==true, we use open to mark if the sub items are shown
        }

        this.handleClick = this.handleClick.bind(this)
        this.handleClickSubItem = this.handleClickSubItem.bind(this)
    }

    getItemStringFromKey = (i18nStrings, entrykey) => {
        switch (entrykey) {
            case 'translation':
                return (this.props.entryLang === 'sv' ? i18nStrings[EN_DEFINITION_TEXT_ID] : i18nStrings[SV_DEFINITION_TEXT_ID]) + this.props.entryDetails
            case 'synonym':
                return i18nStrings[SYNONYM_TEXT_ID] + this.props.entryDetails
            case 'definition':
                return i18nStrings[SV_DEFINITION_TEXT_ID] + this.props.entryDetails
            case 'paradigm':
                return i18nStrings[INFLECTIONS_TEXT_ID] + this.props.entryDetails
            case 'phonetic':
                return i18nStrings[PRONUNCIATION_TEXT_ID] + '[' + this.props.entryDetails['value'] + ']'
            case 'explanation':
                return i18nStrings[EXPLANATION_TEXT_ID] + this.props.entryDetails
            case 'examples': // here we combine all example nodes to a new "examples"/"idioms" field
                return i18nStrings[EXAMPLES_TEXT_ID]
            case 'idioms':
                return i18nStrings[IDIOMS_TEXT_ID]
            case 'compounds':
                return i18nStrings[COMPOUNDS_TEXT_ID]
            default:
                return ''
        }
    }

    setNoNeedForNativeTranslate = () => {
        const { dispatch } = this.props
        dispatch(setNativeLanguage('noNeed'))
        dispatch(cleanHistoryData(DATA_SOURCE_MSAPI))

        saveStringToLocalStorage(LOCAL_STORAGE_KEY_NAME.NATIVE_LANGUAGE.description, 'noNeed')
    }

    showAlertDialog = () => {
        const {onOpenSelectNativeDialog, openAlertDialog, i18nStrings} = this.props

        openAlertDialog(ALERT_TYPE.NORMAL, i18nStrings[SET_NATIVE_LANG_DIALOG_TITLE_ID],
            i18nStrings[SET_NATIVE_LANG_DIALOG_TEXT_ID], [
                {
                    actionText: i18nStrings[SET_NATIVE_LANG_BUTTON_TEXT_ID],
                    action: onOpenSelectNativeDialog
                },
                {
                    actionText: i18nStrings[NO_NEED_TEXT_ID],
                    action: this.setNoNeedForNativeTranslate
                }
            ]
        )
    }

    handleClick() {
        const { dispatch, entryText, entryKey, nativeLanguage, headString, entryDetails, openNativeDialog, openSaolInflectionDialog, setCurrentEntryText } = this.props
        switch (entryKey) {
            case 'translation':
                if (nativeLanguage === '') {// detected browser language is not in the language list
                    this.showAlertDialog()
                } else if (nativeLanguage !== 'noNeed') {
                    this.getNativeFromTranslation(headString, entryDetails, nativeLanguage)
                }
                break
            case 'phonetic':
                this.playPronunciation()
                break
            case 'explanation':
            case 'definition':
                if (nativeLanguage === '') {// detected browser language is not in the language list
                    this.showAlertDialog()
                } else if (nativeLanguage !== 'noNeed') {
                    const originalText = polishText('sv', entryDetails)
                    dispatch(selectDataSource(DATA_SOURCE_MSAPI))

                    dispatch(lookupKey(DATA_SOURCE_MSAPI, MSAPI_REQUEST_TRANS_TYPE, originalText))
                    dispatch(fetchDetailsIfNeeded(DATA_SOURCE_MSAPI, MSAPI_REQUEST_TRANS_TYPE, originalText, 'sv', nativeLanguage))

                    openNativeDialog(entryDetails, entryDetails, 'sv')
                }
                break
            case 'paradigm':
                setCurrentEntryText(entryText)
                dispatch(selectDataSource(DATA_SOURCE_SAOL))

                dispatch(lookupKey(DATA_SOURCE_SAOL, SAOL_REQUEST_INFLECTION_TYPE, this.props.entryText))
                dispatch(fetchDetailsIfNeeded(DATA_SOURCE_SAOL, SAOL_REQUEST_INFLECTION_TYPE, this.props.entryText))

                openSaolInflectionDialog()

                break
            default:
                break
        }

        if (this.props.expandable) {
            this.setState(state =>
                ({
                    ...state,
                    open: !state.open
                }));
        }
    }

    // for the entries of 'examples' and 'idioms', user can click them to translate their English translation
    handleClickSubItem(text, translation) {
        const { nativeLanguage } = this.props
        if (nativeLanguage === '') {// detected browser language is not in the language list
            this.showAlertDialog()
        } else if (nativeLanguage !== 'noNeed') {
            // normally English -> Chinese translation is more accurate than Chinese -> English with MS API
            // so here we will use English text
            const textForMsApi = this.props.entryLang === 'sv' ? translation : text
            const {dispatch} = this.props
            dispatch(selectDataSource(DATA_SOURCE_MSAPI))

            const msApiRequestType = (isStringText(textForMsApi) ? MSAPI_REQUEST_TRANS_TYPE : MSAPI_REQUEST_DICT_TYPE)
            dispatch(lookupKey(DATA_SOURCE_MSAPI, msApiRequestType, textForMsApi))
            dispatch(fetchDetailsIfNeeded(DATA_SOURCE_MSAPI, msApiRequestType, textForMsApi, 'en', nativeLanguage))

            let {openNativeDialog} = this.props
            this.props.entryLang === 'sv' ? openNativeDialog(text, translation, 'en') : openNativeDialog(translation, text, 'en')
        }
    }


     getNativeFromTranslation(text, translation, nativeLang) {
        const {dispatch} = this.props
        dispatch(selectDataSource(DATA_SOURCE_MSAPI))

        let originalText = translation

        let msApiRequestType
        if (this.props.entryLang === 'sv') { // for Swedish entry, the translation is in English. For English -> Chinese, we need to check if it is text or word
            msApiRequestType = (isStringText(translation) ? MSAPI_REQUEST_TRANS_TYPE : MSAPI_REQUEST_DICT_TYPE)
        } else {    // since there is no Swedish -> Chinese dictionary lookup in MS Api, here we just use translate option
            msApiRequestType = MSAPI_REQUEST_TRANS_TYPE
            originalText = polishText('sv', translation)
        }

        dispatch(lookupKey(DATA_SOURCE_MSAPI, msApiRequestType, originalText))
        const srcLang = this.props.entryLang === 'sv' ? 'en' : 'sv'
        dispatch(fetchDetailsIfNeeded(DATA_SOURCE_MSAPI, msApiRequestType, originalText, srcLang, nativeLang))

        let {openNativeDialog} = this.props
        this.props.entryLang === 'sv' ? openNativeDialog(text, translation, srcLang) : openNativeDialog(translation, text, srcLang)
    }

    playPronunciation() {
        this.pronunciation.load()
        this.pronunciation.play();
    }

    render() {
        const { classes, entryKey, entryDetails, i18nStrings } = this.props

        return (
            <List key={entryKey} component="div" disablePadding>
                <ListItem button className={classes.embedded} onClick={this.handleClick}>
                    <ListItemIcon>
                        <ImportContacts />
                    </ListItemIcon>
                    <ListItemText primary={this.getItemStringFromKey(i18nStrings, entryKey)}/>
                    {this.props.expandable && (this.state.open ? <ExpandLess /> : <ExpandMore />)}
                    {
                        entryKey === 'phonetic'
                        && (<div>
                            <VolumeUp />
                            <audio ref={(pronunciation) => { this.pronunciation = pronunciation }}>
                                <source src={getSoundFileUrl(entryDetails)} type="audio/mpeg" >
                                </source>
                            </audio>
                            </div>
                            )
                    }
                    {
                        ( entryKey === 'translation' || entryKey === 'explanation' || entryKey === 'definition' )
                        && (<div>
                                <Translate />
                            </div>
                        )
                    }
                    {
                        ( entryKey === 'paradigm' )
                        && (<div>
                                <More />
                            </div>
                        )
                    }
                </ListItem>
                {this.props.expandable && (
                    <Collapse in={this.state.open} timeout="auto" unmountOnExit >
                        {entryDetails.map((subEntry, i) =>
                            <List key={i} component="div" disablePadding>
                                <ListItem button className={classes.subentry} onClick={() => this.handleClickSubItem(subEntry['value'], subEntry['translation'])}>
                                    <ListItemIcon>
                                        <Book />
                                    </ListItemIcon>
                                    <ListItemText
                                                  primary={subEntry['value']}
                                                  secondary={subEntry['translation']}
                                    />
                                    <div>
                                        <Translate />
                                    </div>
                                </ListItem>
                            </List>
                        )
                        }
                    </Collapse>
                )}
            </List>
        );
    }

}


export default withStyles(styles)(EmbeddedList)