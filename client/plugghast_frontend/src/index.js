import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {logger} from "redux-logger";
import reducer from './reducers/reducers'

import './css/index.css';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';

import { composeWithDevTools } from 'redux-devtools-extension'
import * as actionCreators from './actions/actions'

const middlewares = [thunk]
if (process.env.NODE_ENV !== 'production') {
    middlewares.push(logger)
}

const composeEnhancers = composeWithDevTools({ actionCreators, trace: true, traceLimit: 25 });
const store = createStore(
    reducer,
    composeEnhancers(
        applyMiddleware(...middlewares)
    )
)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
